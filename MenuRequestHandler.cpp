#include "MenuRequestHandler.h"

/*
this method handles the signout request
input: RequestInfo, information about the request
output: RequestResult, the handle response
*/
RequestResult MenuRequestHandler::signout(RequestInfo req)
{
	RequestResult toRet;
	LogoutRespone resp;
	LoginManager::getInstance()->logout(m_user->getUsername());
	resp.status = OK;
	toRet.response = JsonResponsePacketSerializer::serializeResponse(resp);
	toRet.newHandler = RequestHandlerFactory::CreateLoginRequestHandler();
	return toRet;
}

/*
this method handles the get room request
input: RequestInfo, information about the request
output: RequestResult, the handle response
*/
RequestResult MenuRequestHandler::getRooms(RequestInfo req)
{
	RoomManager* roomManager = RoomManager::getInstance();
	RequestResult toRet;
	getRoomsResponse resp;
	resp.rooms = roomManager->getRooms();
	resp.status = OK;
	toRet.response = JsonResponsePacketSerializer::serializeResponse(resp);
	toRet.newHandler = this;
	return toRet;
}

/*
this method handles the get players in room request
input: RequestInfo, information about the request
output: RequestResult, the handle response
*/
RequestResult MenuRequestHandler::getPlayersInRoom(RequestInfo req)
{
	RoomManager* roomManager = RoomManager::getInstance();
	RequestResult toRet;
	getPlayerslnRoomRequest request = JsonRequestPacketDeserializer::deserializeGetPlayersRequest(req.buffer);
	getPlayerslnRoomRespones resp;
	try
	{
		resp.rooms = roomManager->getRoom(request.roomId)->getallusers();
	}
	catch (...)
	{
		throw std::exception(ROOM_NOT_EXISTS_PLAYERS_MSG);
	}
	toRet.response = JsonResponsePacketSerializer::serializeResponse(resp);
	toRet.newHandler = this;
	return toRet;
}

/*
this method handles the get statistics request
input: RequestInfo, information about the request
output: RequestResult, the handle response
*/
RequestResult MenuRequestHandler::getStatistics(RequestInfo req)
{
	StatisticsManager* statisticsManager = StatisticsManager::getInstance();
	RequestResult toRet;
	getStatisticsResponse resp;
	resp.highScores = statisticsManager->getStatistics(m_user->getUsername());
	resp.status = OK;
	toRet.response = JsonResponsePacketSerializer::serializeResponse(resp);
	toRet.newHandler = this;
	return toRet;
}

/*
this method handles the join room request
input: RequestInfo, information about the request
output: RequestResult, the handle response
*/
RequestResult MenuRequestHandler::joinRoom(RequestInfo req)
{
	RoomManager* roomManager = RoomManager::getInstance();
	RequestResult toRet;
	joinRoomRequest request = JsonRequestPacketDeserializer::deserializeJoinRoomRequest(req.buffer);
	joinRoomResponse resp;
	Room* room = roomManager->getRoom(request.roomId);
	room->addUser(m_user);
	resp.status = OK;
	toRet.response = JsonResponsePacketSerializer::serializeResponse(resp);
	toRet.newHandler = RequestHandlerFactory::CreateRoomMemRequestHandler(room, m_user);
	return toRet;
}

/*
this method handles the create room request
input: RequestInfo, information about the request
output: RequestResult, the handle response
*/
RequestResult MenuRequestHandler::createRoom(RequestInfo req)
{
	RoomManager* roomManager = RoomManager::getInstance();
	RequestResult toRet;
	createRoomResquest request = JsonRequestPacketDeserializer::deserializeCreateRoomRequest(req.buffer);
	createRoomResponse resp;
	Room* room = roomManager->getRoom(roomManager->createRoom(m_user, request));
	resp.status = OK;
	toRet.response = JsonResponsePacketSerializer::serializeResponse(resp);
	toRet.newHandler = RequestHandlerFactory::CreateRoomAdRequestHandler(room, m_user);
	return toRet;
}

/*
this method is the C'tor
input: LoggedUser, the user
output: non
*/
MenuRequestHandler::MenuRequestHandler(LoggedUser* user)
{
	m_user = user;
}

/*
this method is the D'tor
input: non
output: non
*/
MenuRequestHandler::~MenuRequestHandler()
{
}

bool MenuRequestHandler::isRequestRelevant(RequestInfo req)
{
	char code = req.code;
	return (code == LOGOUT_CODE || code == GET_ROOMS_CODE || code == GET_PLAYERS_IN_ROOM_CODE
		|| code == GET_STATISTICS_CODE || code == JOIN_ROOM_CODE || code == CREATE_ROOM_CODE);
}

RequestResult MenuRequestHandler::handleRequest(RequestInfo req)
{
	RequestResult toRet;
	try
	{
		switch (req.code)
		{
			case LOGOUT_CODE:
				toRet = signout(req);
				break;
			case GET_ROOMS_CODE:
				toRet = getRooms(req);
				break;
			case GET_PLAYERS_IN_ROOM_CODE:
				toRet = getPlayersInRoom(req);
				break;
			case GET_STATISTICS_CODE:
				toRet = getStatistics(req);
				break;
			case JOIN_ROOM_CODE:
				toRet = joinRoom(req);
				break;
			default:
				toRet = createRoom(req);
				break;
		}
	}
	catch (std::exception e)
	{
		toRet.response = JsonResponsePacketSerializer::buildErrorMsg(e.what());
		toRet.newHandler = this;
	}
	return toRet;
}

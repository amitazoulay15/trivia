#pragma once
#include "defines.h"
#include "structs.h"
#include "JsonResponsePacketSerializer.h"
#include "JsonRequestPacketDeserializer.h"
#include "LoginManager.h"
#include "RoomManager.h"
#include "StatisticsManager.h"
#include "GameManager.h"

class IRequestHandler
{
public:
	virtual bool isRequestRelevant(RequestInfo req);
	virtual RequestResult handleRequest(RequestInfo req);
};
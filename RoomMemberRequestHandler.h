#pragma once
#include "IRequestHandler.h"
#include "RequestHandlerFactory.h"

class RoomMemberRequestHandler : public IRequestHandler
{
private:
	Room* m_room;
	LoggedUser* m_user;
	RequestResult leaveRoom(RequestInfo req);
	RequestResult getRoomState(RequestInfo req);
public:
	RoomMemberRequestHandler(Room* room, LoggedUser* user);
	~RoomMemberRequestHandler();
	virtual bool isRequestRelevant(RequestInfo req) override;
	virtual RequestResult handleRequest(RequestInfo req) override;
};


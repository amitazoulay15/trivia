#pragma once
#include "Communicator.h"

class Server
{
private:
	Communicator* m_communicator;
	static Server* instance;
	static void callCommunicator();
	void closeAllResurces();
	Server();
public:
	static Server* getInstance();
	~Server();
	void run();
};


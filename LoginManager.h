#pragma once
#include "LoggedUser.h"
#include "SqliteDataBase.h"
#include "defines.h"
#include <string>
#include <vector>
#include <iostream>
#include <mutex>

class LoginManager
{

public:
	LoggedUser* signup(std::string username, std::string password, std::string email);
	LoggedUser* login(std::string username, std::string password);
	void logout(std::string username);
	~LoginManager();
	static LoginManager* getInstance();
private:
	LoginManager();
	std::mutex logged_user_mtx;
	SqliteDataBase* m_database;
	std::vector<LoggedUser*> m_loggedUsers;
	static LoginManager* instance;
};


#pragma once
#include <string>
#include <map>
#include "Question.h"
#include <ctime>

class IRequestHandler;

typedef struct RequestResult
{
	char* response;
	IRequestHandler* newHandler;
}RequestResult;

typedef struct CloseGameResponse
{
	unsigned int status;
}CloseGameResponse;

typedef struct RoomData
{
	unsigned int id;
	std::string name;
	unsigned int maxPlayers;
	unsigned int timePerQustion;
	unsigned int isActive;
	unsigned int questionCount;
}RoomData;

typedef struct GameData
{
	unsigned int correctAnswerCount;
	unsigned int wrongAnswerCount;
	unsigned int averageAnswerTime;
	bool onTime;
	bool correct;

}GameData;

typedef struct RequestInfo
{
	char code;
	clock_t receivalTime;
	char* buffer;
}RequestInfo;

typedef struct playerResults
{
	unsigned int correctAnswerCount;
	unsigned int wrongAnswerCount;
	unsigned int avarageAnswerTime;
	std::string username;
}playerResults;

typedef struct LoginRequest
{
	std::string username;
	std::string password;
}LoginRequest;

typedef struct SignupRequest
{
	std::string username;
	std::string password;
	std::string email;
}SignupRequest;

typedef struct createRoomResquest
{
	std::string roomName;
	unsigned int maxUsers;
	unsigned int questionsCount;
	unsigned int answerTimeout;

}createRoomResquest;

typedef struct joinRoomRequest
{
	unsigned int roomId;

}joinRoomRequest;

typedef struct getPlayerslnRoomRequest
{
	unsigned int roomId;

}getPlayerslnRoomRequest;

typedef struct submitAnswerRequest
{
	std::string answer;

}submitAnswerRequest;

typedef struct ErrorResponse
{
	std::string messege;
	char type;
}ErrorResponse;

typedef struct CorrectAnswerMessege
{
	std::string messege;
}CorrectAnswerMessege;

typedef struct LoginResponse
{
	unsigned int status;
}LoginResponse;

typedef struct SignupResponse
{
	unsigned int status;
}SignupResponse;

typedef struct closeRoomResponse
{
	unsigned int status;

}closeRoomResponse;

typedef struct startGameResponse
{
	unsigned int status;

}startGameResponse;

typedef struct getRoomStateRespone
{
	bool hasGameBegun;
	unsigned int status;
	std::vector<std::string> players;
	unsigned int questionCount;
	unsigned int answerTimeout;

}getRoomStateRespone;

typedef struct getRoomsResponse
{
	std::vector<RoomData> rooms;
	unsigned int status;

}getRoomsResponse;

typedef struct getPlayerslnRoomRespones
{
	std::vector<std::string> rooms;

}getPlayersslnRoomRespones;

typedef struct LogoutRespone
{
	unsigned int status;

}LogoutRespone;

typedef struct LeaveRoomResponse
{
	unsigned int status;

}LeaveRoomResponse;

typedef struct GetQuestionResponse
{
	unsigned int status;
	std::string question;
	std::vector<std::string> answers;

}GetQuestionResponse;

typedef struct submitAnswerResponse
{
	unsigned int status;

}submitAnswerResponse;

typedef struct getGameResultsResponse
{
	unsigned int status;
	std::vector<playerResults> results;

}getGameResultsResponse;

typedef struct createRoomResponse
{
	unsigned int status;
}createRoomResponse;

typedef struct joinRoomResponse
{
	unsigned int status;

}joinRoomResponse;

typedef struct getStatisticsResponse
{
	unsigned int status;
	std::vector<std::string> highScores;

}getStatisticsResponse;

typedef struct LeaveGameResponse
{
	unsigned int status;
}LeaveGameResponse;
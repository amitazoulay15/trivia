#include "IRequestHandler.h"

/*
this function checks if the messege that the server recived is the one it should get
input: the request informaion
output: bool, if the request is valid or not
*/
bool IRequestHandler::isRequestRelevant(RequestInfo req)
{
	throw std::exception("this is the father class");
}

/*
this function handles the information recived in the request and manages the client
input: the information about the request
output: the result of the handling
*/
RequestResult IRequestHandler::handleRequest(RequestInfo req)
{
	throw std::exception("this is the father class");
}

#include "Question.h"

/*
this method is the C'tor
input: 2 string, the correct answer and the question, vector of string, all the wring answers
output: non
*/
Question::Question(std::string question, std::string correct, std::vector<std::string> possibleAnswers)
{
	m_correct = correct;
	m_possibleAnswers = possibleAnswers;
	m_question = question;
}

/*
this method is the D'tor
input: non
output: non
*/
Question::~Question()
{
}

/*
this method return the question
input: non
output: string, the question
*/
std::string Question::getQuestion()
{
	return m_question;
}

/*
this method returns all the wrong answers of the questions
input: non
output: vector of strings, all the wrong answer
*/
std::vector<std::string> Question::getPossibleAnswers()
{
	return m_possibleAnswers;
}

/*
this method return the correct answer to the question
input: non
output: string, the corect answer
*/
std::string Question::getCorrentAnswer()
{
	return m_correct;
}

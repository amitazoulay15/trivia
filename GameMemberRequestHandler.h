#pragma once
#include "IRequestHandler.h"
#include "RequestHandlerFactory.h"

class GameMemberRequestHandler : public IRequestHandler
{
private:
	Game* m_game;
	LoggedUser* m_user;
	RequestResult submitAnswer(RequestInfo req);
	RequestResult leaveGame(RequestInfo req);
public:
	GameMemberRequestHandler(Game* game, LoggedUser* user);
	~GameMemberRequestHandler();
	virtual bool isRequestRelevant(RequestInfo req) override;
	virtual RequestResult handleRequest(RequestInfo req) override;
};


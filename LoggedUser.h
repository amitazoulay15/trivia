#pragma once
#include <string>
#include <WinSock2.h>
#include "WSAInitializer.h"
#include "SocketAndMtx.h"

class LoggedUser
{
public:
	LoggedUser(std::string username);
	LoggedUser(std::string username, SocketAndMtx* sock);
	void setName(std::string username);
	LoggedUser();
	~LoggedUser();
	std::string getUsername();
	void sendMessege(char* messege);
	SocketAndMtx* getSocket();
	void setSocket(SocketAndMtx* sock);
private:
	SocketAndMtx* m_sock;
	std::string m_username;
};

#include "Room.h"

/*
this method is the C'tor
input: RoomData, data for the room, LoggedUser*, the first user to add to the room (the owner)
output: non
*/
Room::Room(RoomData metadata, LoggedUser* user)
{
	m_metadata = metadata;
	this->addUser(user);
}

/*
this method is the D'tor
input: non
output: non
*/
Room::~Room()
{
}

/*
this method adds a player to the room
input: pointer to the player (LoggedUser*)
output: non
*/
void Room::addUser(LoggedUser* user)
{
	if (m_users.size() < m_metadata.maxPlayers)
	{
		users_mtx.lock();
		this->m_users.push_back(user);
		users_mtx.unlock();
		std::string msg = user->getUsername() + " just joined the room!" + POPUP_MESSEGE;
		char* messege = JsonResponsePacketSerializer::buildErrorMsg(msg);
		this->sendMessegeToRoomMembers(messege, user);
	}
	else
	{
		throw std::exception(ROOM_REACHED_MAX_PLAYERS_MSG);
	}
}

/*This function remove user from the room
input user : user to remove from the room
type user : LoggedUser*
return : void*/
void Room::removeUser(LoggedUser* user)
{
	users_mtx.lock();
	for (int i = 0; i < m_users.size(); i++)
	{
		if (m_users[i] == user)
		{
			this->m_users.erase(m_users.begin() + i);
		}
	}
	users_mtx.unlock();
	std::string msg = user->getUsername() + " just left the room!" + POPUP_MESSEGE;
	char* messege = JsonResponsePacketSerializer::buildErrorMsg(msg);
	this->sendMessegeToRoomMembers(messege);
}
/*This function all users in the room
input : void
return allUsersVec : vector with the all users
rtype : std::vector<std::string> */
std::vector<std::string> Room::getallusers()
{
	std::vector<std::string> allUsersVec;
	users_mtx.lock();
	for (int i = 0; i < m_users.size(); i++)
	{
		allUsersVec.push_back(m_users[i]->getUsername());
	}
	users_mtx.unlock();
	return allUsersVec;
}

/*
this method return the data of the room
input: non
output: RoomData*
*/
RoomData* Room::getData()
{
	return (&this->m_metadata);
}

/*
this method return all the logged players that are in the room
input: non
output: vector of LoggedUser*, the players
*/
std::vector<LoggedUser*> Room::getAllLoggedUsers()
{
	return m_users;
}

/*
this method send a messege to all the players in the room
input: char*, the messege to send
output: non
*/
void Room::sendMessegeToRoomMembers(char* mes, LoggedUser* admin)
{
	int msgLen = strlen(mes);
	users_mtx.lock();
	for (int i = 0; i < m_users.size(); i++)
	{
		if (m_users[i] != admin)
		{
			char* msg = new char[msgLen + 1];
			msg[msgLen] = 0;
			for (int j = 0; j < msgLen; j++)
				msg[j] = mes[j];
			m_users[i]->sendMessege(msg);
		}
	}
	users_mtx.unlock();
}

std::mutex* Room::getMtx()
{
	return &users_mtx;
}

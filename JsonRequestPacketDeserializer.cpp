#include "JsonRequestPacketDeserializer.h"
#include <iostream>

/*
this method desiralizes the LoginRequest
input: the buffer
output: the LoginRequest object of the data
*/
LoginRequest JsonRequestPacketDeserializer::deserializeLoginRequest(char* msg)
{
	std::string msg_as_string = msg;
	LoginRequest toRet;
	nlohmann::json j = nlohmann::json::parse(msg_as_string);
	toRet.username = j["username"];
	toRet.password = j["password"];
	delete[] msg;
	return toRet;
}

/*
this method desiralizes the SignupRequest
input: the buffer
output: the SignupRequest object of the data
*/
SignupRequest JsonRequestPacketDeserializer::deserializeSignUpRequest(char* msg)
{
	std::string msg_as_string = msg;
	SignupRequest toRet;
	nlohmann::json j = nlohmann::json::parse(msg_as_string);
	toRet.username = j["username"];
	toRet.password = j["password"];
	toRet.email = j["email"];
	delete[] msg;
	return toRet;
}

/*
this method deserializes the getPlayerslnRoomRequest request
input: char*, the request data
output: getPlayerslnRoomRequest
*/
getPlayerslnRoomRequest JsonRequestPacketDeserializer::deserializeGetPlayersRequest(char* msg)
{
	std::string msg_as_string = msg;
	getPlayerslnRoomRequest toRet;
	nlohmann::json j = nlohmann::json::parse(msg_as_string);
	toRet.roomId = j["roomId"];
	delete[] msg;
	return toRet;
}

/*
this method deserializes the joinRoomRequest request
input: char*, the request data
output: joinRoomRequest
*/
joinRoomRequest JsonRequestPacketDeserializer::deserializeJoinRoomRequest(char* msg)
{
	std::string msg_as_string = msg;
	joinRoomRequest toRet;
	nlohmann::json j = nlohmann::json::parse(msg_as_string);
	toRet.roomId = j["roomId"];
	delete[] msg;
	return toRet;
}

/*
this method deserializes the createRoomResquest request
input: char*, the request data
output: createRoomResquest
*/
createRoomResquest JsonRequestPacketDeserializer::deserializeCreateRoomRequest(char* msg)
{
	std::string msg_as_string = msg;
	createRoomResquest toRet;
	nlohmann::json j = nlohmann::json::parse(msg_as_string);
	toRet.roomName = j["roomName"];
	toRet.maxUsers = j["maxUsers"];
	toRet.questionsCount = j["questionsCount"];
	toRet.answerTimeout = j["answerTimeout"];
	delete[] msg;
	return toRet;
}

/*
this method deserializes the submitAnswerRequest request
input: char*, the request data
output: submitAnswerRequest
*/
submitAnswerRequest JsonRequestPacketDeserializer::deserializeSubmitAnswerRequest(char* msg)
{
	std::string msg_as_string = msg;
	submitAnswerRequest toRet;
	nlohmann::json j = nlohmann::json::parse(msg_as_string);
	toRet.answer = j["answer"];
	delete[] msg;
	return toRet;
}
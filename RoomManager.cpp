#include "RoomManager.h"

RoomManager* RoomManager::instance = nullptr;

/*
this method is the C'tor
input: non
output: non
*/
RoomManager::RoomManager()
{
}

/*
this method is the D'tor
input: non
output: non
*/
RoomManager::~RoomManager()
{
	rooms_mtx.lock();
	for (int i = 0; i < m_rooms.size(); i++)//deleting all the rooms
		delete m_rooms[i];
	rooms_mtx.unlock();
}

/*
this method creates a room
input: the admin of the room, the request to create a room
output: non
*/
int RoomManager::createRoom(LoggedUser* user, createRoomResquest req)
{
	RoomData data;
	data.isActive = true;
	data.maxPlayers = req.maxUsers;
	data.name = req.roomName;
	data.questionCount = req.questionsCount;
	data.timePerQustion = req.answerTimeout;
	rooms_mtx.lock();
	data.id = m_rooms.size() + 1;
	m_rooms.push_back(new Room(data, user));
	rooms_mtx.unlock();
	return data.id;
}

/*
this method deletes a room
input: the id of the room (int), the messege to send to all the user in the room (string)
output: non
*/
void RoomManager::deleteRoom(int id, std::string messege, LoggedUser* sender)
{
	Room* r = nullptr;
	rooms_mtx.lock();
	for (int i = 0; i < m_rooms.size(); i++)
	{
		if (m_rooms[i]->getData()->id == id)
		{
			r = m_rooms[i];
			m_rooms.erase(m_rooms.begin() + i);
			break;
		}
	}
	rooms_mtx.unlock();
	char* send = JsonResponsePacketSerializer::buildErrorMsg(messege);
	r->sendMessegeToRoomMembers(send, sender);
	delete r;
}

/*
this method gets the room state
input: the id of the room (int)
output: unsigned int, the room state
*/
unsigned int RoomManager::getRoomState(int id)
{
	return getRoom(id)->getData()->isActive;
}

/*
this method build data about all the rooms that exists 
input: non
output: vector of roomdata, data about each room in the server
*/
std::vector<RoomData> RoomManager::getRooms()
{
	std::vector<RoomData> toRet;
	rooms_mtx.lock();
	for (int i = 0; i < m_rooms.size(); i++)
		toRet.push_back((*m_rooms[i]->getData()));
	rooms_mtx.unlock();
	return toRet;
}

/*
this method returns a room
input: the id of the room (int)
output: room*, pointer to the asked room
*/
Room* RoomManager::getRoom(int id)
{
	Room* toRet = nullptr;
	rooms_mtx.lock();
	for (int i = 0; i < m_rooms.size(); i++)
	{
		if (m_rooms[i]->getData()->id == id)
		{
			toRet = m_rooms[i];
		}
	}
	rooms_mtx.unlock();
	if (toRet != nullptr) return toRet;
	throw std::exception(ROOM_NOT_EXISTS_MSG);
}

/*
this method is for the singelton
input: non
output: RoomManager*, the instance of the class
*/
RoomManager* RoomManager::getInstance()
{
	if (instance == nullptr) instance = new RoomManager();
	return instance;
}

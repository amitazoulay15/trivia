#include "RoomMemberRequestHandler.h"

/*
this method handles leave room request
input: RequestInfo, the request information
output: RequestResult, the request resault
*/
RequestResult RoomMemberRequestHandler::leaveRoom(RequestInfo req)
{
	RequestResult toRet;
	LeaveRoomResponse resp;
	m_room->removeUser(m_user);//removing the user from the room
	resp.status = OK;
	toRet.response = JsonResponsePacketSerializer::serializeResponse(resp);
	toRet.newHandler = RequestHandlerFactory::CreateMenuRequestHandler(m_user);
	return toRet;
}

/*
this method handles the get room state request
input: RequestInfo, the request information
output: RequestResult, the request resault
*/
RequestResult RoomMemberRequestHandler::getRoomState(RequestInfo req)
{
	RequestResult toRet;
	getRoomStateRespone resp;
	//getting the room data
	resp.answerTimeout = m_room->getData()->timePerQustion;
	resp.hasGameBegun = m_room->getData()->isActive;
	resp.players = m_room->getallusers();
	resp.questionCount = m_room->getData()->questionCount;
	resp.status = OK;
	toRet.response = JsonResponsePacketSerializer::serializeResponse(resp);
	toRet.newHandler = this;
	return toRet;
}

/*
this method is the C'tor
input: Room*, pointer to the room the player is at, LoggedUser*, pointer to the user
output: non
*/
RoomMemberRequestHandler::RoomMemberRequestHandler(Room* room, LoggedUser* user)
{
	m_room = room;
	m_user = user;
}

/*
this method is the D'to
input: non
output: non
*/
RoomMemberRequestHandler::~RoomMemberRequestHandler()
{
}

bool RoomMemberRequestHandler::isRequestRelevant(RequestInfo req)
{
	char code = req.code;
	return (code == LEAVE_ROOM_CODE || code == GET_ROOM_STATE_CODE);
}

RequestResult RoomMemberRequestHandler::handleRequest(RequestInfo req)
{
	RequestResult toRet;
	try
	{
		if (req.code == LEAVE_ROOM_CODE) toRet = leaveRoom(req);
		else toRet = getRoomState(req);
	}
	catch (std::exception e)
	{
		toRet.response = JsonResponsePacketSerializer::buildErrorMsg(e.what());
		toRet.newHandler = this;
	}
	return toRet;
}

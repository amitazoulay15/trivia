#include "GameAdminRequestHandler.h"

/*
this method handles the submit answer request
input: RequestInfo, information about the request
output: RequestResult, the handle response
*/
RequestResult GameAdminRequestHandler::submitAnswer(RequestInfo req)
{
	RequestResult toRet;
	submitAnswerRequest data = JsonRequestPacketDeserializer::deserializeSubmitAnswerRequest(req.buffer);
	submitAnswerResponse resp;
	m_game->submitAnswer(m_user, data.answer, req.receivalTime);
	resp.status = OK;
	toRet.newHandler = this;
	toRet.response = JsonResponsePacketSerializer::serializeResponse(resp);
	return toRet;
}

/*
this method handles the close game request
input: RequestInfo, information about the request
output: RequestResult, the handle response
*/
RequestResult GameAdminRequestHandler::closeGame(RequestInfo req)
{
	RequestResult toRet;
	CloseGameResponse resp;
	Communicator* ComInstance = Communicator::getInstance();
	m_game->getMtx()->lock();
	std::map<LoggedUser*, GameData*> users = m_game->getUsersMap();
	std::map<LoggedUser*, GameData*>::iterator i = users.begin();
	for (i = i; i != users.end(); ++i)
	{
		if (i->first != m_user)
		{
			IRequestHandler* newHandler = RequestHandlerFactory::CreateMenuRequestHandler(i->first);
			ComInstance->updateRequestHandler(i->first->getSocket(), newHandler);
		}
	}
	m_game->getMtx()->unlock();
	GameManager::getInstance()->deleteGame(m_game, m_user);
	resp.status = OK;
	toRet.newHandler = RequestHandlerFactory::CreateMenuRequestHandler(m_user);
	toRet.response = JsonResponsePacketSerializer::serializeResponse(resp);
	return toRet;
}

/*
this method is the C'tor
input: Game*, the game the player is in, LoggedUser*, the user
output: non
*/
GameAdminRequestHandler::GameAdminRequestHandler(Game* game, LoggedUser* user)
{
	m_game = game;
	m_user = user;
}

/*
this method is the D'tor
input: non
output: non
*/
GameAdminRequestHandler::~GameAdminRequestHandler()
{
}

bool GameAdminRequestHandler::isRequestRelevant(RequestInfo req)
{
	char code = req.code;
	return (code == CLOSE_GAME_CODE || code == SUBMIT_ANSWER_CODE);
}

RequestResult GameAdminRequestHandler::handleRequest(RequestInfo req)
{
	RequestResult toRet;
	try
	{
		if (req.code == CLOSE_GAME_CODE) toRet = closeGame(req);
		else toRet = submitAnswer(req);
	}
	catch (std::exception e)
	{
		toRet.response = JsonResponsePacketSerializer::buildErrorMsg(e.what());
		toRet.newHandler = this;
	}
	return toRet;
}

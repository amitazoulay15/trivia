#include "LoginManager.h"

LoginManager* LoginManager::instance = nullptr;

/*This function access DB to see if the user exists and the password
Correct and connects the user (adds to the user list
Connected)
input username,password,email : username,password,email of user
type username,password,email: string
return : void*/
LoggedUser* LoginManager::signup(std::string username, std::string password, std::string email)
{
	if (!this->m_database->doesUserExist(username))
	{
		this->m_database->addNewUser(username, password, email);
		LoggedUser* toRet = new LoggedUser(username);
		logged_user_mtx.lock();
		this->m_loggedUsers.push_back(toRet);
		logged_user_mtx.unlock();
		return toRet;
	}
	throw std::exception(USER_EXISTS_MSG);
}

/*This function get all the user information (username,password,email) and signup  the user
in the DB.
input username,password,email : username,password of user
type username,password: string
return : void*/
LoggedUser* LoginManager::login(std::string username, std::string password)
{
	if (this->m_database->doesUserExist(username) == true)
	{
		if (this->m_database->doesPasswordMatch(username, password) == true)
		{
			LoggedUser* toRet = new LoggedUser(username);
			logged_user_mtx.lock();
			this->m_loggedUsers.push_back(toRet);
			logged_user_mtx.unlock();
			return toRet;
		}
		throw std::exception(NOT_CORRECT_MSG);
	}
	throw std::exception(USER_NOT_EXISTS_MSG);
}

/*This function logout user(It is removed from the list of connected users)
input username : username of user
type username : string
return : void*/
void LoginManager::logout(std::string username)
{
	bool found = false;
	logged_user_mtx.lock();
	for (int i = 0; i < m_loggedUsers.size(); i++)
	{
		if (m_loggedUsers[i]->getUsername() == username)
		{
			this->m_loggedUsers.erase(m_loggedUsers.begin() + i);
			found = true;
		}
	}
	logged_user_mtx.unlock();
	if (!found)
		throw std::exception(USER_NOT_LOGGED_MSG);
}

/*
this method is the C'tor
input: non
output: non
*/
LoginManager::LoginManager()
{
	m_database = SqliteDataBase::getInstance();//getting instance of the DB
}

/*
this method is the D'tor
input: non
output: non
*/
LoginManager::~LoginManager()
{
	logged_user_mtx.lock();
	for (int i = 0; i < m_loggedUsers.size(); i++)//deleting all the user pointers in the system
		delete m_loggedUsers[i];
	logged_user_mtx.unlock();
}

/*
this method is for the singelton
input: non
output: LoginManager*, pointer to the only instance of the class
*/
LoginManager* LoginManager::getInstance()
{
	if (instance == nullptr) instance = new LoginManager();
	return instance;
}

#include "Communicator.h"

Communicator* Communicator::instance = nullptr;

/*
this method binds and starts the listen of the server on the preset port
input: non
output: non
*/
void Communicator::bindAndListen()
{
	m_serverSock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	int port = PORT;
	struct sockaddr_in sa = { 0 };
	sa.sin_port = htons(port);
	sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = INADDR_ANY;
	if (bind(m_serverSock, (struct sockaddr*) & sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");
	if (listen(m_serverSock, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
}

/*
this method handles new clients
input: the socket-RequestHandler map, Socket of the client
output: non
*/
void Communicator::handleNewClient(std::map<SocketAndMtx*, IRequestHandler*>& clients, SocketAndMtx* sock, std::mutex& mtx)
{
	std::time_t recivel_time;
	RequestInfo info;
	RequestResult toSend;
	char* finalSend;
	mtx.lock();
	IRequestHandler* handler = clients[sock];
	mtx.unlock();
	while (handler != nullptr)
	{
		try//if the user disconnected or the server deleted him
		{
			info = sock->recvMsgFromSock();
		}
		catch (...) { break; }
		info.receivalTime = clock();
		mtx.lock();
		if (clients[sock] != handler)
			handler = clients[sock];
		mtx.unlock();
		if (handler->isRequestRelevant(info))
		{
			toSend = handler->handleRequest(info);
			finalSend = toSend.response;
		}
		else
		{
			toSend.newHandler = handler;
			finalSend = JsonResponsePacketSerializer::buildErrorMsg("Request is not relevent!c");
		}
		if (toSend.newHandler != handler)
		{
			mtx.lock();
			clients[sock] = toSend.newHandler;
			delete handler;
			handler = clients[sock];
			mtx.unlock();
		}
		sock->sendMsg(finalSend);
	}
	mtx.lock();
	std::map<SocketAndMtx*, IRequestHandler*>::iterator i = clients.find(sock);
	if (i != clients.end())//if the server deleted it, it doesnt exist
	{
		delete clients[sock];//if there was a problem with the client i need to make sure the server can clean its memory
		clients[sock] = nullptr;
	}
	mtx.unlock();
}

void Communicator::cleanUsers(std::map<SocketAndMtx*, IRequestHandler*>& clients, std::mutex& mtx, std::pair <bool, std::mutex>& m_work)
{
	mtx.lock();
	std::map<SocketAndMtx*, IRequestHandler*>::iterator it = clients.begin();
	mtx.unlock();
	while (true)
	{
		int i = 0;
		m_work.second.lock();
		if (!m_work.first)
			break;
		m_work.second.unlock();
		mtx.lock();
		for (std::map<SocketAndMtx*, IRequestHandler*>::iterator j = it; j != clients.end() && i < COUNTER_FOR_CLEANING;)
		{
			if (j->second == nullptr)
			{
				delete j->first;
				clients.erase(j++);
			}
			else
			{
				++j;
				it = j;
			}
			i++;
		}
		if (clients.size() == 0 || clients.size() == 1)
		{
			it = clients.begin();
		}
		mtx.unlock();
	}
	m_work.second.unlock();//if breaking need to release
}

/*
this method accepts new clients to the server
input: non
output: non
*/
void Communicator::accept()
{
	try
	{
		SOCKET* client_socket = new SOCKET(::accept(m_serverSock, NULL, NULL));
		if (*client_socket == INVALID_SOCKET)
			throw std::exception(__FUNCTION__);
		IRequestHandler* rqHandle = RequestHandlerFactory::CreateLoginRequestHandler();
		SocketAndMtx* sockAndMtx = new SocketAndMtx(client_socket);
		m_client_mtx.lock();
		m_clients.insert(std::pair<SocketAndMtx*, IRequestHandler*>(sockAndMtx, rqHandle));
		m_client_mtx.unlock();
		std::thread newClient(handleNewClient, std::ref(m_clients), sockAndMtx, std::ref(m_client_mtx));
		newClient.detach();
	}
	catch (...)
	{
		std::cout << "error while accepting new client" << std::endl;
	}
}

Communicator::~Communicator()
{
	m_work.second.lock();
	m_work.first = false;
	m_work.second.unlock();
	std::string messege = SERVER_CLOSED_MSG;
	char* toSend = JsonResponsePacketSerializer::buildErrorMsg(messege);
	//delete all the sockets and handlers
	m_client_mtx.lock();
	for (std::map<SocketAndMtx*, IRequestHandler*>::iterator i = m_clients.begin(); i != m_clients.end(); ++i)
	{
		char* msg = new char[strlen(toSend) + 1];
		msg[strlen(toSend)] = 0;
		for (int i = 0; i < strlen(toSend); i++)
			msg[i] = toSend[i];
		i->first->sendMsg(msg);//sending message to all the users
		delete i->second;
		delete i->first;
	}
	m_clients.clear();
	m_client_mtx.unlock();
	delete GameManager::getInstance();
	delete LoginManager::getInstance();
	delete RoomManager::getInstance();
	delete StatisticsManager::getInstance();
	delete SqliteDataBase::getInstance();
}

void Communicator::updateRequestHandler(SocketAndMtx* sock, IRequestHandler* newHandler)
{
	m_client_mtx.lock();
	IRequestHandler* toDel = m_clients[sock];
	m_clients[sock] = newHandler;
	m_client_mtx.unlock();
	delete toDel;
}

SocketAndMtx* Communicator::getSock(IRequestHandler* handler)
{
	m_client_mtx.lock();
	for (std::map<SocketAndMtx*, IRequestHandler*>::iterator i = m_clients.begin(); i != m_clients.end(); ++i)
	{
		if (handler == i->second)
		{
			SocketAndMtx* toRet = i->first;
			m_client_mtx.unlock();
			return toRet;
		}
	}
	m_client_mtx.unlock();
}

Communicator* Communicator::getInstance()
{
	if (instance == nullptr) instance = new Communicator();
	return instance;
}

Communicator::Communicator()
{
	m_work.first = true;
	bindAndListen();
	std::thread cleanUsersThread(cleanUsers, std::ref(m_clients), std::ref(m_client_mtx), std::ref(m_work));
	cleanUsersThread.detach();
}

/*
this method is the methid that runs and waits for new clients
input: non
output: non
*/
void Communicator::startHandleRequests()
{
	while (true)
	{
		accept();
	}
}

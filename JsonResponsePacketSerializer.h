#pragma once
#include "structs.h"
#include "json.hpp"
#include "defines.h"

class JsonResponsePacketSerializer
{
private:
	static char* buildFullMsg(std::string msg, char code);
	static std::string getRoomsString(std::vector<RoomData> rooms);
	static std::string getPlayersString(std::vector<std::string> names);
	static std::string getplayersResualtsString(std::vector<playerResults> results);
	static std::string deleteQuates(std::string starting);
public:
	static char* buildErrorMsg(std::string msg);
	static std::string buildNumWithPedding(int num, int length);
	static char* serializeResponse(ErrorResponse res);
	static char* serializeResponse(LoginResponse res);
	static char* serializeResponse(SignupResponse res);
	static char* serializeResponse(LogoutRespone res);
	static char* serializeResponse(getRoomsResponse res);
	static char* serializeResponse(getPlayerslnRoomRespones res);
	static char* serializeResponse(joinRoomResponse res);
	static char* serializeResponse(createRoomResponse res);
	static char* serializeResponse(getStatisticsResponse res);
	static char* serializeResponse(closeRoomResponse res);
	static char* serializeResponse(startGameResponse res);
	static char* serializeResponse(getRoomStateRespone res);
	static char* serializeResponse(LeaveRoomResponse res);
	static char* serializeResponse(submitAnswerResponse res);
	static char* serializeResponse(getGameResultsResponse res);
	static char* serializeResponse(GetQuestionResponse res);
	static char* serializeResponse(CloseGameResponse res);
	static char* serializeResponse(LeaveGameResponse res);
	static char* serializeResponse(CorrectAnswerMessege res);
};


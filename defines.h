#pragma once
#define OK 0
#define NOT_OK 1
#define LENGTH 4
#define SERVER_CLOSE '-'
#define ERROR_CODE '0'
#define LOGIN_CODE '1'
#define SIGNUP_CODE '2'
#define LOGOUT_CODE '3'
#define GET_PLAYERS_IN_ROOM_CODE '4'
#define GET_ROOMS_CODE '5'
#define LEAVE_GAME_CODE '6'
#define GET_STATISTICS_CODE '7'
#define GET_GAME_RESULTS_CODE '8'
#define CREATE_ROOM_CODE '9'
#define JOIN_ROOM_CODE 'q'
#define LEAVE_ROOM_CODE 'w'
#define GET_QUESTION_CODE 'e'
#define SUBMIT_ANSWER_CODE 'r'
#define GET_ROOM_STATE_CODE 't'
#define CLOSE_ROOM_CODE 'y'
#define START_GAME_CODE 'u'
#define CLOSE_GAME_CODE 'i'
#define CORRECT_ANSWER_MESSEGE 'o'
#define POPUP_MESSEGE 'p'
#define	REQUEST_NO_RElEVENT 'c'
#define LAST_CHAR_BEFORE_DATA 5
#define STOP_CMD "EXIT"
#define PORT 1234
#define ROOM_ACTIVE 0
#define ROOM_NOT_ACTIVE 1
#define	BEST_PLAYERS_AMOUNT 5
#define	GAME_STARTED_MSG "The game has been started"
#define	NOT_CORRECT_MSG "User or password is not correct!1"
#define	USER_NOT_EXISTS_MSG "User does not exists!1"
#define	USER_EXISTS_MSG "User exists already!2"
#define	USER_NOT_LOGGED_MSG "User not logged!3"
#define	ROOM_NOT_EXISTS_PLAYERS_MSG "The room does not exists!4"
#define	ROOM_CLOSED_BY_OWNER_MSG "The owner closed the room!y"
#define	ROOM_NOT_EXISTS_MSG "The room does not exists!q"
#define	GAME_STARTED_MSG "The Game has begun!u"
#define ROOM_REACHED_MAX_PLAYERS_MSG "the room reached max players!q"
#define GAME_CLOSED_BY_OWNER_MSG "The gme has been closed by the owner!i"
#define SERVER_CLOSED_MSG "The server has been closed!-"
#define ERROR_SQL "Error running command"
#define	COUNTER_FOR_CLEANING 5
#define	SECONDS_TO_MILI 1000
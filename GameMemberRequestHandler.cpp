#include "GameMemberRequestHandler.h"

/*
this method handles the submit answer request
input: RequestInfo, information about the request
output: RequestResult, the handle response
*/
RequestResult GameMemberRequestHandler::submitAnswer(RequestInfo req)
{
	RequestResult toRet;
	submitAnswerRequest data = JsonRequestPacketDeserializer::deserializeSubmitAnswerRequest(req.buffer);
	submitAnswerResponse resp;
	m_game->submitAnswer(m_user, data.answer, req.receivalTime);
	resp.status = OK;
	toRet.newHandler = this;
	toRet.response = JsonResponsePacketSerializer::serializeResponse(resp);
	return toRet;
}

/*
this method handles the leave game request
input: RequestInfo, information about the request
output: RequestResult, the handle response
*/
RequestResult GameMemberRequestHandler::leaveGame(RequestInfo req)
{
	RequestResult toRet;
	LeaveGameResponse resp;
	m_game->removeUser(m_user);
	resp.status = OK;
	toRet.newHandler = RequestHandlerFactory::CreateMenuRequestHandler(m_user);
	toRet.response = JsonResponsePacketSerializer::serializeResponse(resp);
	return toRet;
}

/*
this method is the C'tor
input:
output: non
*/
GameMemberRequestHandler::GameMemberRequestHandler(Game* game, LoggedUser* user)
{
	m_game = game;
	m_user = user;
}

/*
this method is the D'tor
input: non
output: non
*/
GameMemberRequestHandler::~GameMemberRequestHandler()
{
}

bool GameMemberRequestHandler::isRequestRelevant(RequestInfo req)
{
	char code = req.code;
	return (code == LEAVE_GAME_CODE || code == SUBMIT_ANSWER_CODE);
}

RequestResult GameMemberRequestHandler::handleRequest(RequestInfo req)
{
	RequestResult toRet;
	try
	{
		if (req.code == LEAVE_GAME_CODE) toRet = leaveGame(req);
		else toRet = submitAnswer(req);
	}
	catch (std::exception e)
	{
		toRet.response = JsonResponsePacketSerializer::buildErrorMsg(e.what());
		toRet.newHandler = this;
	}
	return toRet;
}

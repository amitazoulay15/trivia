#pragma once
#include "IRequestHandler.h"
#include "RequestHandlerFactory.h"

class MenuRequestHandler : public IRequestHandler
{
private:
	LoggedUser* m_user;
	RequestResult signout(RequestInfo req);
	RequestResult getRooms(RequestInfo req);
	RequestResult getPlayersInRoom(RequestInfo req);
	RequestResult getStatistics(RequestInfo req);
	RequestResult joinRoom(RequestInfo req);
	RequestResult createRoom(RequestInfo req);
public:
	MenuRequestHandler(LoggedUser* user);
	~MenuRequestHandler();
	virtual bool isRequestRelevant(RequestInfo req) override;
	virtual RequestResult handleRequest(RequestInfo req) override;
};


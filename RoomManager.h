#pragma once
#include "LoggedUser.h"
#include "Room.h"
#include <map>
#include <iterator>
#include <iostream>

class RoomManager
{
public:
	~RoomManager();
	int createRoom(LoggedUser* user, createRoomResquest req);
	void deleteRoom(int id, std::string messege, LoggedUser* sender = nullptr);
	unsigned int getRoomState(int id);
	std::vector<RoomData> getRooms();
	Room* getRoom(int id);
	static RoomManager* getInstance();
private:
	RoomManager();
	std::mutex rooms_mtx;
	std::vector<Room*> m_rooms;
	static RoomManager* instance;
};


#include "RoomAdminRequestHandler.h"

/*
this method handles the close room request
input: RequestInfo, the requests information
output: RequestResult, the requests resault
*/
RequestResult RoomAdminRequestHandler::closeRoom(RequestInfo req)
{
	RequestResult toRet;
	closeRoomResponse resp;
	m_room->getMtx()->lock();
	std::vector<LoggedUser*> members = m_room->getAllLoggedUsers();//getting all the players in the room
	RoomManager* roomManager = RoomManager::getInstance();
	for (int i = 0; i < members.size(); i++)//changing all the players handlers to menu handler
	{
		if (members[i] != m_user)
		{
			IRequestHandler* newHandler = RequestHandlerFactory::CreateMenuRequestHandler(members[i]);
			Communicator::getInstance()->updateRequestHandler(members[i]->getSocket(), newHandler);
		}
	}
	m_room->getMtx()->unlock();
	roomManager->deleteRoom(m_room->getData()->id, ROOM_CLOSED_BY_OWNER_MSG, m_user);//deleting the room
	resp.status = OK;
	toRet.response = JsonResponsePacketSerializer::serializeResponse(resp);
	toRet.newHandler = RequestHandlerFactory::CreateMenuRequestHandler(m_user);
	return toRet;
}

/*
this method handles the start game request
input: RequestInfo, the requests information
output: RequestResult, the requests resault
*/
RequestResult RoomAdminRequestHandler::startGame(RequestInfo req)
{
	RoomManager* roomManager = RoomManager::getInstance();
	RequestResult toRet;
	startGameResponse resp;
	int id = m_room->getData()->id;
	Game* game = GameManager::getInstance()->createGame(m_room);
	m_room->getMtx()->lock();
	std::vector<LoggedUser*> members = m_room->getAllLoggedUsers();//getting all the users
	for (int i = 0; i < members.size(); i++)//changing the handler of all the players in the room
	{
		if (members[i] != m_user)
		{
			IRequestHandler* newHandler = RequestHandlerFactory::CreateGameMemRequestHandler(game, members[i]);
			Communicator::getInstance()->updateRequestHandler(members[i]->getSocket(), newHandler);
		}
	}
	m_room->getMtx()->unlock();
	roomManager->deleteRoom(id, GAME_STARTED_MSG, m_user);//deleting the room
	resp.status = OK;
	toRet.response = JsonResponsePacketSerializer::serializeResponse(resp);
	toRet.newHandler = RequestHandlerFactory::CreateGameAdRequestHandler(game, m_user);
	game->start();
	return toRet;
}

/*
this method handles the get room state request
input: RequestInfo, the requests information
output: RequestResult, the requests resault
*/
RequestResult RoomAdminRequestHandler::getRoomState(RequestInfo req)
{
	RequestResult toRet;
	getRoomStateRespone resp;
	resp.answerTimeout = m_room->getData()->timePerQustion;
	resp.hasGameBegun = m_room->getData()->isActive;
	resp.players = m_room->getallusers();
	resp.questionCount = m_room->getData()->questionCount;
	resp.status = OK;
	toRet.response = JsonResponsePacketSerializer::serializeResponse(resp);
	toRet.newHandler = this;
	return toRet;
}

/*
this method is the C'tor
input: Room*, pointer to the room the player is at, LoggedUser*, pointer to the player's data
output: non
*/
RoomAdminRequestHandler::RoomAdminRequestHandler(Room* room, LoggedUser* user)
{
	m_user = user;
	m_room = room;
}

/*
this method is the D'tor
input: non
output: non
*/
RoomAdminRequestHandler::~RoomAdminRequestHandler()
{
}

bool RoomAdminRequestHandler::isRequestRelevant(RequestInfo req)
{
	char code = req.code;
	return (code == CLOSE_ROOM_CODE || code == START_GAME_CODE || code == GET_ROOM_STATE_CODE);
}

RequestResult RoomAdminRequestHandler::handleRequest(RequestInfo req)
{
	RequestResult toRet;
	try
	{
		switch (req.code)
		{
		case CLOSE_ROOM_CODE:
			toRet = closeRoom(req);
			break;
		case START_GAME_CODE:
			toRet = startGame(req);
			break;
		default:
			toRet = getRoomState(req);
			break;
		}
	}
	catch (std::exception e)
	{
		toRet.response = JsonResponsePacketSerializer::buildErrorMsg(e.what());
		toRet.newHandler = this;
	}
	return toRet;
}

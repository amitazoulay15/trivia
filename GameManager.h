#pragma once
#include "Game.h"
#include "Room.h"
#include "SqliteDataBase.h"

class GameManager
{
public:
	static GameManager* getInstance();
	~GameManager();
	Game* createGame(Room* room);
	void deleteGame(Game* game, LoggedUser* sender = nullptr);
private:
	GameManager();
	static GameManager* instance;
	std::mutex m_games_mtx;
	std::vector<Game*> m_games;
};


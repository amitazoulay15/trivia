#pragma once
#include "IRequestHandler.h"
#include "Communicator.h"
#include "RequestHandlerFactory.h"

class LoginRequestHandler : public IRequestHandler
{
private:
	LoggedUser* m_user;
	RequestResult login(RequestInfo req);
	RequestResult signup(RequestInfo req);
public:
	LoginRequestHandler();
	~LoginRequestHandler();
	virtual bool isRequestRelevant(RequestInfo req) override;
	virtual RequestResult handleRequest(RequestInfo req) override;
};


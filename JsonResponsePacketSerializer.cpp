#include "JsonResponsePacketSerializer.h"
#include <iostream>

/*
this method build the whole messege by given arguments
input: string that is the messege(data part of whole messege), char the code of the messege
output: char* the final messege
*/
char* JsonResponsePacketSerializer::buildFullMsg(std::string msg, char code)
{
	std::string toRet = "";
	int len = msg.length();
	toRet += code;
	toRet += buildNumWithPedding(len, LENGTH);
	toRet += msg;
	len = toRet.length();
	char* ret = new char[len+1];
	ret[len] = 0;
	for (int i = 0; i < len; i++)
	{
		ret[i] = toRet[i];
	}
	return ret;
}

/*
this method builds a string of all the room in the server
input: std::vector<RoomData>, vector of all the room data
output: std::string, the resault of the combination of all the data in a string
*/
std::string JsonResponsePacketSerializer::getRoomsString(std::vector<RoomData> rooms)
{
	std::string toRet = "";
	for (int i = 0; i < rooms.size(); i++)
	{
		nlohmann::json j;
		j["id"] = rooms[i].id;
		j["name"] = rooms[i].name;
		j["maxPlayers"] = rooms[i].maxPlayers;
		j["timePerQustion"] = rooms[i].timePerQustion;
		j["isActive"] = rooms[i].isActive;
		j["questionCount"] = rooms[i].questionCount;
		std::string ret = j.dump();
		toRet += ret + ',';
	}
	if (toRet != "")
		toRet.pop_back();
	return '[' + toRet + ']';
}

/*
this method builds a string of all the names of all the given names
input: std::vector<std::string>, all the names provided to the function
output: std::string, a string of all the names combined
*/
std::string JsonResponsePacketSerializer::getPlayersString(std::vector<std::string> names)
{
	std::string toRet = "";
	for (int i = 0; i < names.size(); i++)
	{
		toRet += '\"' + names[i] + "\",";
	}
	if (toRet != "")
		toRet.pop_back();
	return '['+toRet+']';
}

/*
this method builds a string of all the playerResaults given
input: std::vector<playerResults>, the resaults to buil the string from
output: std::string, the product of the method
*/
std::string JsonResponsePacketSerializer::getplayersResualtsString(std::vector<playerResults> results)
{
	std::string toRet = "";
	for (int i = 0; i < results.size(); i++)
	{
		playerResults pr = results[i];
		nlohmann::json j;
		j["correctAnswerCount"] = pr.correctAnswerCount;
		j["wrongAnswerCount"] = pr.wrongAnswerCount;
		j["avarageAnswerTime"] = pr.avarageAnswerTime;
		j["username"] = pr.username;
		toRet += j.dump() + ',';
	}
	toRet.pop_back();
	return toRet;
}

std::string JsonResponsePacketSerializer::deleteQuates(std::string starting)
{
	std::string toRet = "";
	int add = 0;
	for (int i = 0; i < starting.length(); i++)
	{
		i += add;
		add = 0;
		if (starting[i] == '[')
			toRet.pop_back();
		else if (starting[i] == ']')
			add = 1;
		else if (starting[i] == '\\')
			continue;
		toRet += starting[i];
	}
	return toRet;
}

/*
this static method builds a char array of a given number with pedding before it
input: the number to convert, the overall length
output: the char array of the number with pedding
*/
char* JsonResponsePacketSerializer::buildErrorMsg(std::string msg)
{
	ErrorResponse resp;
	resp.type = msg[msg.size() - 1];
	msg.pop_back();
	resp.messege = msg;
	return serializeResponse(resp);
}

std::string JsonResponsePacketSerializer::buildNumWithPedding(int num, int length)
{
	std::string toRet = "";
	int len = 0, temp = num;
	while (temp != 0)
	{
		len++;
		temp /= 10;
	}
	int i = 0;
	for (i = 0; i < length - len; i++)
	{
		toRet += '0';
	}
	toRet += std::to_string(num);
	return toRet;
}

/*
this method serializes a ErrorResponse
input: ErrorResponse struct
output: array of chars
*/
char* JsonResponsePacketSerializer::serializeResponse(ErrorResponse res)
{
	nlohmann::json j;
	j["messege"] = res.messege;
	std::string ret = j.dump() + res.type;
	char* toRet = buildFullMsg(ret, ERROR_CODE);
	return toRet;
}

/*
this method serializes a LoginResponse
input: LoginResponse struct
output: array of chars
*/
char* JsonResponsePacketSerializer::serializeResponse(LoginResponse res)
{
	nlohmann::json j;
	j["status"] = res.status;
	std::string ret = j.dump();
	char* toRet = buildFullMsg(ret, LOGIN_CODE);
	return toRet;
}

/*
this method serializes a SignupResponse
input: SignupResponse struct
output: array of chars
*/
char* JsonResponsePacketSerializer::serializeResponse(SignupResponse res)
{
	nlohmann::json j;
	j["status"] = res.status;
	std::string ret = j.dump();
	char* toRet = buildFullMsg(ret, SIGNUP_CODE);
	return toRet;
}

/*
this method serializes the LogoutRespone reponse
input: LogoutRespone, the response to serialize
output: char*, array of bytes of the answer
*/
char* JsonResponsePacketSerializer::serializeResponse(LogoutRespone res)
{
	nlohmann::json j;
	j["status"] = res.status;
	std::string ret = j.dump();
	char* toRet = buildFullMsg(ret, LOGOUT_CODE);
	return toRet;
}

/*
this method serializes the getRoomsResponse reponse
input: getRoomsResponse, the response to serialize
output: char*, array of bytes of the answer
*/
char* JsonResponsePacketSerializer::serializeResponse(getRoomsResponse res)
{
	nlohmann::json j;
	j["status"] = res.status;
	j["rooms"] = getRoomsString(res.rooms);
	std::string ret = j.dump();
	ret = deleteQuates(ret);
	char* toRet = buildFullMsg(ret, GET_ROOMS_CODE);
	return toRet;
}

/*
this method serializes the getPlayerslnRoomRespones reponse
input: getPlayerslnRoomRespones, the response to serialize
output: char*, array of bytes of the answer
*/
char* JsonResponsePacketSerializer::serializeResponse(getPlayerslnRoomRespones res)
{
	nlohmann::json j;
	j["players"] = getPlayersString(res.rooms);
	std::string ret = j.dump();
	ret = deleteQuates(ret);
	char* toRet = buildFullMsg(ret, GET_PLAYERS_IN_ROOM_CODE);
	return toRet;
}

/*
this method serializes the joinRoomResponse reponse
input: joinRoomResponse, the response to serialize
output: char*, array of bytes of the answer
*/
char* JsonResponsePacketSerializer::serializeResponse(joinRoomResponse res)
{
	nlohmann::json j;
	j["status"] = res.status;
	std::string ret = j.dump();
	char* toRet = buildFullMsg(ret, JOIN_ROOM_CODE);
	return toRet;
}

/*
this method serializes the createRoomResponse reponse
input: createRoomResponse, the response to serialize
output: char*, array of bytes of the answer
*/
char* JsonResponsePacketSerializer::serializeResponse(createRoomResponse res)
{
	nlohmann::json j;
	j["status"] = res.status;
	std::string ret = j.dump();
	char* toRet = buildFullMsg(ret, CREATE_ROOM_CODE);
	return toRet;
}

/*
this method serializes the getStatisticsResponse reponse
input: getStatisticsResponse, the response to serialize
output: char*, array of bytes of the answer
*/
char* JsonResponsePacketSerializer::serializeResponse(getStatisticsResponse res)
{
	nlohmann::json j;
	j["status"] = res.status;
	j["scores"] = getPlayersString(res.highScores);
	std::string ret = j.dump();
	ret = deleteQuates(ret);
	char* toRet = buildFullMsg(ret, GET_STATISTICS_CODE);
	return toRet;
}

/*
this method serializes the closeRoomResponse reponse
input: closeRoomResponse, the response to serialize
output: char*, array of bytes of the answer
*/
char* JsonResponsePacketSerializer::serializeResponse(closeRoomResponse res)
{
	nlohmann::json j;
	j["status"] = res.status;
	std::string ret = j.dump();
	char* toRet = buildFullMsg(ret, CLOSE_ROOM_CODE);
	return toRet;
}

/*
this method serializes the startGameResponse reponse
input: startGameResponse, the response to serialize
output: char*, array of bytes of the answer
*/
char* JsonResponsePacketSerializer::serializeResponse(startGameResponse res)
{
	nlohmann::json j;
	j["status"] = res.status;
	std::string ret = j.dump();
	char* toRet = buildFullMsg(ret, START_GAME_CODE);
	return toRet;
}

/*
this method serializes the getRoomStateRespone reponse
input: getRoomStateRespone, the response to serialize
output: char*, array of bytes of the answer
*/
char* JsonResponsePacketSerializer::serializeResponse(getRoomStateRespone res)
{
	nlohmann::json j;
	j["status"] = res.status;
	j["hasGameBegun"] = res.hasGameBegun;
	j["players"] = getPlayersString(res.players);
	j["questionCount"] = res.questionCount;
	j["answerTimeout"] = res.answerTimeout;
	std::string ret = j.dump();
	ret = deleteQuates(ret);
	char* toRet = buildFullMsg(ret, GET_ROOM_STATE_CODE);
	return toRet;
}

/*
this method serializes the LeaveRoomResponse reponse
input: LeaveRoomResponse, the response to serialize
output: char*, array of bytes of the answer
*/
char* JsonResponsePacketSerializer::serializeResponse(LeaveRoomResponse res)
{
	nlohmann::json j;
	j["status"] = res.status;
	std::string ret = j.dump();
	char* toRet = buildFullMsg(ret, LEAVE_ROOM_CODE);
	return toRet;
}

/*
this method serializes the submitAnswerResponse reponse
input: submitAnswerResponse, the response to serialize
output: char*, array of bytes of the answer
*/
char* JsonResponsePacketSerializer::serializeResponse(submitAnswerResponse res)
{
	nlohmann::json j;
	j["status"] = res.status;
	std::string ret = j.dump();
	char* toRet = buildFullMsg(ret, SUBMIT_ANSWER_CODE);
	return toRet;
}

/*
this method serializes the getGameResultsResponse reponse
input: getGameResultsResponse, the response to serialize
output: char*, array of bytes of the answer
*/
char* JsonResponsePacketSerializer::serializeResponse(getGameResultsResponse res)
{
	nlohmann::json j;
	j["status"] = res.status;
	j["results"] = '[' + getplayersResualtsString(res.results) + ']';
	std::string ret = j.dump();
	ret = deleteQuates(ret);
	char* toRet = buildFullMsg(ret, GET_GAME_RESULTS_CODE);
	return toRet;
}

/*
this method serializes the GetQuestionResponse reponse
input: GetQuestionResponse, the response to serialize
output: char*, array of bytes of the answer
*/
char* JsonResponsePacketSerializer::serializeResponse(GetQuestionResponse res)
{
	nlohmann::json j;
	j["status"] = res.status;
	j["question"] = res.question;
	j["answers"] = getPlayersString(res.answers);
	std::string ret = j.dump();
	ret = deleteQuates(ret);
	char* toRet = buildFullMsg(ret, GET_QUESTION_CODE);
	return toRet;
}

/*
this method serializes the CloseGameResponse reponse
input: CloseGameResponse, the response to serialize
output: char*, array of bytes of the answer
*/
char* JsonResponsePacketSerializer::serializeResponse(CloseGameResponse res)
{
	nlohmann::json j;
	j["status"] = res.status;
	std::string ret = j.dump();
	char* toRet = buildFullMsg(ret, CLOSE_GAME_CODE);
	return toRet;
}

/*
this method serializes the LeaveGameResponse reponse
input: LeaveGameResponse, the response to serialize
output: char*, array of bytes of the answer
*/
char* JsonResponsePacketSerializer::serializeResponse(LeaveGameResponse res)
{
	nlohmann::json j;
	j["status"] = res.status;
	std::string ret = j.dump();
	char* toRet = buildFullMsg(ret, LEAVE_GAME_CODE);
	return toRet;
}

/*
this method serializes the CorrectAnswerMessege reponse
input: CorrectAnswerMessege, the response to serialize
output: char*, array of bytes of the answer
*/
char* JsonResponsePacketSerializer::serializeResponse(CorrectAnswerMessege res)
{
	nlohmann::json j;
	j["answer"] = '\"' + res.messege + '\"';
	std::string ret = j.dump();
	char* toRet = buildFullMsg(ret, CORRECT_ANSWER_MESSEGE);
	return toRet;
}

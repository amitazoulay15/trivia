#include "GameManager.h"

GameManager* GameManager::instance = nullptr;

/*
this method this is the C'tor
input: non
output: non
*/
GameManager::GameManager()
{
}

/*
this method is for the singelton
input: non
output: GameManager*, pointer to the only instance of the class
*/
GameManager* GameManager::getInstance()
{
	if (instance == nullptr) instance = new GameManager();
	return instance;
}

/*
this method is the D'tor
input: non
output: non
*/
GameManager::~GameManager()
{
	m_games_mtx.lock();
	for (int i = 0; i < m_games.size(); i++)
		delete m_games[i];
	m_games_mtx.unlock();
}

/*
this method creates new game
input: Room*, the room to change into a game
output: Game*, pointer to the game that was created
*/
Game* GameManager::createGame(Room* room)
{
	Game* game = new Game(room->getAllLoggedUsers(), *room->getData());
	m_games_mtx.lock();
	m_games.push_back(game);
	m_games_mtx.unlock();
	return game;
}

/*
this method deletes game
input: Game*, a pointer to the game that it need to delete
output: non
*/
void GameManager::deleteGame(Game* game, LoggedUser* sender)
{
	m_games_mtx.lock();
	for (int i = 0; i < m_games.size(); i++)
	{
		if (m_games[i] == game)
		{
			m_games.erase(m_games.begin() + i);
			break;
		}
	}
	m_games_mtx.unlock();
	std::string msg = GAME_CLOSED_BY_OWNER_MSG;
	char* messege = JsonResponsePacketSerializer::buildErrorMsg(msg);
	game->sendMessegeToGameMembers(messege, sender);
	delete game;
}

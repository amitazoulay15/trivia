﻿using Newtonsoft.Json;
using System;
using System.Windows;

namespace gui
{
    public partial class Menu : Window
    {
        private Communicator com;

        public Menu(ref Communicator comm)
        {
            com = comm;
            InitializeComponent();
        }

        private void logoutButton(object sender, RoutedEventArgs e)
        {
            string toSend = defines.LOGOUT_CODE + "0000";
            baseMsg ans = com.sendAndRecv(toSend, defines.LOGOUT_CODE);
            if (ans != null)
            {
                foreach (Window window in Application.Current.Windows)
                {
                    if (window.Name != "MainWindow") window.Hide();
                    else window.Show();
                }
            }
        }

        private void createRoomButton(object sender, RoutedEventArgs e)
        {
            createRoomResquest req = new createRoomResquest();
            try
            {
                req.maxUsers = (uint)int.Parse(maxUsers.Text);
                if (req.maxUsers > 0)
                {

                    req.questionsCount = (uint)int.Parse(questionCount.Text);
                    req.roomName = roomName.Text;
                    req.answerTimeout = (uint)int.Parse(answerTimeout.Text);
                    string toSend = Communicator.buildMsg(defines.CREATE_ROOM_CODE, JsonConvert.SerializeObject(req, Formatting.Indented));
                    baseMsg ans = com.sendAndRecv(toSend, defines.CREATE_ROOM_CODE);
                    if (ans != null)
                    {
                        foreach (Window window in Application.Current.Windows)
                        {
                            if (window.Name != "RoomWindow") window.Hide();
                            else window.Show();
                        }
                    }
                }
                else 
                {
                    MessageBox.Show("Room max players must be at least 1!");
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Incorrect input type");
            }
        }

        private void refresh(object sender, RoutedEventArgs e)
        {
            string toSend = defines.GET_ROOMS_CODE + "0000";
            baseMsg ans = com.sendAndRecv(toSend, defines.GET_ROOMS_CODE);
            if (ans != null)
            {
                roomsList.Items.Clear();
                getRoomsResponse resp = JsonConvert.DeserializeObject<getRoomsResponse>(ans.msg);
                for (int i = 0; i < resp.rooms.Count; i++)
                    roomsList.Items.Add(resp.rooms[i].ToString());
            }
            toSend = defines.GET_STATISTICS_CODE + "0000";
            ans = com.sendAndRecv(toSend, defines.GET_STATISTICS_CODE);
            if (ans != null)
            {
                getStatisticsResponse statistics = JsonConvert.DeserializeObject<getStatisticsResponse>(ans.msg);
                one.Text = statistics.scores[0];
                two.Text = statistics.scores[1];
                three.Text = statistics.scores[2];
                four.Text = statistics.scores[3];
                five.Text = statistics.scores[4];
                you.Text = statistics.scores[5];
            }
        }

        private void joinRoomButton(object sender, RoutedEventArgs e)
        {
            joinRoomRequest req = new joinRoomRequest();
            try
            {
                req.roomId = (uint)int.Parse(roomId.Text);
                string toSend = Communicator.buildMsg(defines.JOIN_ROOM_CODE, JsonConvert.SerializeObject(req, Formatting.Indented));
                baseMsg ans = com.sendAndRecv(toSend, defines.JOIN_ROOM_CODE);
                if (ans != null)
                {
                    foreach (Window window in Application.Current.Windows)
                    {
                        if (window.Name != "RoomWindow") window.Hide();
                        else window.Show();
                    }
                }
            }
            catch(Exception)
            {
                MessageBox.Show("Incorrect input type");
            }
        }

        private void getPlayersInRoom(object sender, RoutedEventArgs e)
        {
            getPlayerslnRoomRequest req = new getPlayerslnRoomRequest();
            try
            {
                req.roomId = (uint)int.Parse(roomId.Text);
                string toSend = Communicator.buildMsg(defines.GET_PLAYERS_IN_ROOM_CODE, JsonConvert.SerializeObject(req, Formatting.Indented));
                baseMsg ans = com.sendAndRecv(toSend, defines.GET_PLAYERS_IN_ROOM_CODE);
                if (ans != null)
                {
                    players.Items.Clear();
                    getPlayerslnRoomRespones resp = JsonConvert.DeserializeObject<getPlayerslnRoomRespones>(ans.msg);
                    for (int i = 0; i < resp.players.Count; i++)
                        players.Items.Add(resp.players[i]);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Incorrect input type");
            }
        }
    }
}

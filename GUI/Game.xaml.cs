﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows;
using System.Windows.Controls;

namespace gui
{
    public partial class Game : Window
    {
        private Communicator com;
        private Thread getQuestionsThread;

        private static List<string> shuffleList(List<string> list)
        {
            Random rnd = new Random();
            for (int i = list.Count; i > 0; i--)
            {
                int j = rnd.Next(0, i);
                string temp = list[j];
                list[j] = list[0];
                list[0] = temp;
            }
            return list;
        }

        private static void change(Button b, string s)
        {
            b.Dispatcher.Invoke(() =>
            {
                b.Content = s;
            });
        }

        private static void questionsHandling(ref Communicator com,
            TextBlock question, 
            Button ans1,
            Button ans2,
            Button ans3,
            Button ans4,
            ListBox resaultsBox)
        {
            while (true)
            {
                List<baseMsg> gameResaults = new List<baseMsg>();
                while (gameResaults.Count == 0)
                {
                    List<baseMsg> qs = com.getMsgOfType(defines.GET_QUESTION_CODE);
                    gameResaults = com.getMsgOfType(defines.GET_GAME_RESULTS_CODE);
                    if (qs.Count > 0)
                    {
                        baseMsg q = qs[0];
                        GetQuestionResponse resp = JsonConvert.DeserializeObject<GetQuestionResponse>(q.msg);
                        resp.answers = shuffleList(resp.answers);
                        question.Dispatcher.Invoke(() =>
                        {
                            question.Text = resp.question;
                        });
                        change(ans1, resp.answers[0]);
                        change(ans2, resp.answers[1]);
                        change(ans3, resp.answers[2]);
                        change(ans4, resp.answers[3]);
                    }
                }
                baseMsg gameRes = gameResaults[0];
                getGameResultsResponse final = JsonConvert.DeserializeObject<getGameResultsResponse>(gameRes.msg);
                resaultsBox.Dispatcher.Invoke(() =>
                {
                    for (int i = 0; i < final.results.Count; i++)
                        resaultsBox.Items.Add(final.results[i].ToString());
                });
            }
        }

        public Game(ref Communicator comm)
        {
            com = comm;
            InitializeComponent();
            getQuestionsThread = new Thread(() => questionsHandling(ref com, question, ans1, ans2, ans3, ans4, playersRes));
            getQuestionsThread.IsBackground = true;
            getQuestionsThread.Start();
        }

        private void CloseGame(object sender, RoutedEventArgs e)
        {
            string toSend = defines.CLOSE_GAME_CODE + "0000";
            baseMsg ans = com.sendAndRecv(toSend, defines.CLOSE_GAME_CODE);
            if (ans != null)
            {
                foreach (Window window in Application.Current.Windows)
                {
                    if (window.Name != "MenuWindow") window.Hide();
                    else window.Show();
                }
            }
            playersRes.Items.Clear();
        }

        private void LeaveGame(object sender, RoutedEventArgs e)
        {

            string toSend = defines.LEAVE_GAME_CODE + "0000";
            baseMsg ans = com.sendAndRecv(toSend, defines.LEAVE_GAME_CODE);
            if (ans != null)
            {
                foreach (Window window in Application.Current.Windows)
                {
                    if (window.Name != "MenuWindow") window.Hide();
                    else window.Show();
                }
            }
        }

        private void sendAns(string answer)
        {
            submitAnswerRequest req = new submitAnswerRequest();
            req.answer = answer;
            string toSend = Communicator.buildMsg(defines.SUBMIT_ANSWER_CODE, JsonConvert.SerializeObject(req, Formatting.None));
            com.sendAndRecv(toSend, defines.SUBMIT_ANSWER_CODE);//dont need to do anythimg if the request recived
        }

        private void answer1(object sender, RoutedEventArgs e)
        {
            sendAns(ans1.Content.ToString());
        }

        private void answer2(object sender, RoutedEventArgs e)
        {
            sendAns(ans2.Content.ToString());
        }

        private void answer3(object sender, RoutedEventArgs e)
        {
            sendAns(ans3.Content.ToString());
        }

        private void answer4(object sender, RoutedEventArgs e)
        {
            sendAns(ans4.Content.ToString());
        }
    }
}

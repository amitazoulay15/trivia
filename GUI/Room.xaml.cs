﻿using Newtonsoft.Json;
using System.Linq;
using System.Windows;

namespace gui
{
    public partial class Room : Window
    {
        private Communicator com;

        public Room(ref Communicator comm)
        {
            com = comm;
            InitializeComponent();
        }

        private void leaveRoom(object sender, RoutedEventArgs e)
        {
            string toSend = defines.LEAVE_ROOM_CODE + "0000";
            baseMsg ans = com.sendAndRecv(toSend, defines.LEAVE_ROOM_CODE);
            if (ans != null)
            {
                foreach (Window window in Application.Current.Windows)
                {
                    if (window.Name != "MenuWindow") window.Hide();
                    else window.Show();
                }
            }
        }

        private void closeRoom(object sender, RoutedEventArgs e)
        {
            string toSend = defines.CLOSE_ROOM_CODE + "0000";
            baseMsg ans = com.sendAndRecv(toSend, defines.CLOSE_ROOM_CODE);
            if (ans != null)
            {
                foreach (Window window in Application.Current.Windows)
                {
                    if (window.Title != "MenuWindow") window.Hide();
                    else window.Show();
                }
            }
        }

        private void startGame(object sender, RoutedEventArgs e)
        {
            string toSend = defines.START_GAME_CODE + "0000";
            baseMsg ans = com.sendAndRecv(toSend, defines.START_GAME_CODE);
            if (ans != null)
            {
                foreach (Window window in Application.Current.Windows)
                {
                    if (window.Title != "GameWindow") window.Hide();
                    else window.Show();
                }
            }
        }

        private void refresh(object sender, RoutedEventArgs e)
        {
            string toSend = defines.GET_ROOM_STATE_CODE + "0000";
            baseMsg ans = com.sendAndRecv(toSend, defines.GET_ROOM_STATE_CODE);
            if (ans != null)
            {
                getRoomStateRespone resp = JsonConvert.DeserializeObject<getRoomStateRespone>(ans.msg);
                timePerQuestion.Text = "Time Per Question: " + resp.answerTimeout.ToString();
                questionCount.Text = "Question Count: " + resp.questionCount.ToString();
                playersList.Items.Clear();
                for (int i = 0; i < resp.players.Count(); i++)
                    playersList.Items.Add(resp.players[i]);
            }
        }
    }
}

﻿using gui;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows;

public class Communicator
{
    private Socket m_socket;
    private List<baseMsg> recived;
    private Mutex m_recivedMtx;
    private Thread threadListener;
    private Thread fatalErrors;

    public Communicator()
	{
        m_recivedMtx = new Mutex();
        recived = new List<baseMsg>();
        m_socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        m_socket.Connect(defines.IP,defines.PORT);
        if (!m_socket.Connected)
        {
            throw new Exception();
        }
        threadListener = new Thread(() => listenSocket(ref m_socket, ref recived, ref m_recivedMtx));
        threadListener.IsBackground = true;
        threadListener.Start();
        fatalErrors = new Thread(() => ChangingScreensAndPopUpHandling(ref recived, ref m_recivedMtx));
        fatalErrors.IsBackground = true;
        fatalErrors.Start();
    }

    static private void listenSocket(ref Socket sock, ref List<baseMsg> recived, ref Mutex m_recivedMtx)
    {
        try
        {
            while (true)
            {
                string length = "", code = "", data = "";
                baseMsg msg = new baseMsg();
                Byte[] bytesRecivedCode = new Byte[1];
                Byte[] bytesRecivedLength = new Byte[4];
                Byte[] bytesRecived;
                //reciving the code and length
                sock.Receive(bytesRecivedCode, bytesRecivedCode.Length, 0);
                sock.Receive(bytesRecivedLength, bytesRecivedLength.Length, 0);
                code = Encoding.ASCII.GetString(bytesRecivedCode);
                length = Encoding.ASCII.GetString(bytesRecivedLength);
                //reciving the data
                bytesRecived = new Byte[int.Parse(length)];
                sock.Receive(bytesRecived, bytesRecived.Length, 0);
                data = Encoding.ASCII.GetString(bytesRecived);
                if (code[0] == defines.ERROR_CODE)
                {
                    msg.error = true;
                    msg.type = data[data.Length - 1];
                    data = data.Remove(data.Length - 1);
                    msg.msg = data;
                }
                else
                {
                    msg.error = false;
                    msg.type = code[0];
                    msg.msg = data;
                }
                //adding the data to the list
                m_recivedMtx.WaitOne();
                recived.Add(msg);
                m_recivedMtx.ReleaseMutex();
            }
        }
        catch(Exception)
        {
            MessageBox.Show("Lost conection to server!");
            Application.Current.Dispatcher.Invoke(() =>
            {
                Application.Current.Shutdown();
            });
        }
    }

    public baseMsg sendAndRecv(string msg, char type)
    {
        sendMsg(msg);
        List<baseMsg> answer;
        List<baseMsg> answer1;
        baseMsg ans = new baseMsg();
        do
        {
            answer = getMsgOfType(type);
            answer1 = getMsgOfType(defines.REQUEST_NO_RElEVENT);
        } while (answer.Count == 0 && answer1.Count == 0);
        if (answer.Count > 0)
            ans = answer[0];
        else
            ans = answer1[0];
        if (ans.error)
        {
            ErrorResponse err = JsonConvert.DeserializeObject<ErrorResponse>(ans.msg);
            MessageBox.Show(err.messege);
            return null;
        }
        return ans;
    }

    public List<baseMsg> getMsgOfType(char type)
    {
        List<baseMsg> toRet = new List<baseMsg>();
        m_recivedMtx.WaitOne();
        for(int i = 0; i < recived.Count; i++)
        {
            if (recived[i].type == type)
            {
                toRet.Add(recived[i]);
                recived.Remove(recived[i]);
            }
        }
        m_recivedMtx.ReleaseMutex();
        return toRet;
    }

    static private void ChangingScreensAndPopUpHandling(ref List<baseMsg> recived, ref Mutex m_recivedMtx)
    {
        while (true)
        {
            m_recivedMtx.WaitOne();
            for (int i = 0; i < recived.Count; i++)
            {
                if (recived[i].error && (
                    recived[i].type == defines.CLOSE_ROOM_CODE || 
                    recived[i].type == defines.START_GAME_CODE || 
                    recived[i].type == defines.CLOSE_GAME_CODE ||
                    recived[i].type == defines.SERVER_CLOSE || 
                    recived[i].type == defines.POPUP_CODE)
                    )
                {
                    string show = "";
                    ErrorResponse err = JsonConvert.DeserializeObject<ErrorResponse>(recived[i].msg);
                    MessageBox.Show(err.messege);
                    if (recived[i].type == defines.CLOSE_ROOM_CODE)
                        show = "MenuWindow";
                    else if (recived[i].type == defines.START_GAME_CODE)
                        show = "GameWindow";
                    else if (recived[i].type == defines.CLOSE_GAME_CODE)
                        show = "MenuWindow";
                    else if (recived[i].type == defines.SERVER_CLOSE)
                    {
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            Application.Current.Shutdown();
                        });
                    }
                    if (show != "")
                    {
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            foreach (Window window in Application.Current.Windows)
                            {
                                if (window.Name != show) window.Hide();
                                else window.Show();
                            }
                        });
                    }
                    recived.Remove(recived[i]);
                }
            }
            m_recivedMtx.ReleaseMutex();
        }
    }

    public void sendMsg(string msg)
    {
        Byte[] bytesSent = Encoding.ASCII.GetBytes(msg);
        m_socket.Send(bytesSent, bytesSent.Length, 0);
    }

    public static string buildMsg(char code, string msg)
    {
        string toRet = "" + code;
        string lenMsg = (msg.Length).ToString();
        for (int i = 0; i < 4 - lenMsg.Length; i++)
        {
            toRet += '0';
        }
        toRet += lenMsg + msg;
        return toRet;
    }
}

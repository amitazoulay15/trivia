﻿using Newtonsoft.Json;
using System;
using System.Threading;
using System.Windows;

namespace gui
{
    public partial class Main : Window
    {
        private Communicator com;
        private Game game;
        private Room room;
        private Menu menu;

        public Main()
        {
            InitializeComponent();
            try
            {
                com = new Communicator();
                game = new Game(ref com);
                room = new Room(ref com);
                menu = new Menu(ref com);
                game.Hide();
                room.Hide();
                menu.Hide();
            }
            catch (Exception)
            {
                MessageBox.Show("couldnt connect to server");
                Application.Current.Shutdown();
            }
        }

        private void LoginClick(object sender, RoutedEventArgs e)
        {
            LoginRequest req = new LoginRequest();
            req.username = username.Text;
            req.password = password.Password;
            string toSend = Communicator.buildMsg(defines.LOGIN_CODE, JsonConvert.SerializeObject(req, Formatting.None));
            baseMsg ans = com.sendAndRecv(toSend, defines.LOGIN_CODE);
            if(ans != null)
            {
                Hide();
                menu.Show();
            }
        }

        private void SignupClick(object sender, RoutedEventArgs e)
        {
            SignupRequest req = new SignupRequest();
            req.username = username.Text;
            req.password = password.Password;
            req.email = mail.Text;
            string toSend = Communicator.buildMsg(defines.SIGNUP_CODE, JsonConvert.SerializeObject(req, Formatting.None));
            baseMsg ans = com.sendAndRecv(toSend, defines.SIGNUP_CODE);
            if (ans != null)
            {
                Hide();
                menu.Show();
            }
        }
    }
}

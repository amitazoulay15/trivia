﻿using System.Collections.Generic;

static class defines
{
	public const uint OK = 0;
	public const uint NOT_OK = 1;
	public const char SERVER_CLOSE = '-';
	public const char ERROR_CODE = '0';
	public const char LOGIN_CODE = '1';
	public const char SIGNUP_CODE = '2';
	public const char LOGOUT_CODE = '3';
	public const char GET_PLAYERS_IN_ROOM_CODE = '4';
	public const char GET_ROOMS_CODE = '5';
	public const char LEAVE_GAME_CODE = '6';
	public const char GET_STATISTICS_CODE = '7';
	public const char GET_GAME_RESULTS_CODE = '8';
	public const char CREATE_ROOM_CODE = '9';
	public const char JOIN_ROOM_CODE = 'q';
	public const char LEAVE_ROOM_CODE = 'w';
	public const char GET_QUESTION_CODE = 'e';
	public const char SUBMIT_ANSWER_CODE = 'r';
	public const char GET_ROOM_STATE_CODE = 't';
	public const char CLOSE_ROOM_CODE = 'y';
	public const char START_GAME_CODE = 'u';
	public const char CLOSE_GAME_CODE = 'i';
	public const char CORRECT_ANSWER_MESSEGE = 'o';
	public const char POPUP_CODE = 'p';
	public const char REQUEST_NO_RElEVENT = 'c';
	public const string IP = "127.0.0.1";
	public const int PORT = 1234;
}
public class baseMsg
{
	public string msg { get; set; }
	public bool error { get; set; }
	public char type { get; set; }
}
public class playerResults
{
	public uint correctAnswerCount { get; set; }
	public uint wrongAnswerCount { get; set; }
	public uint avarageAnswerTime { get; set; }
	public string username { get; set; }

	public override string ToString()
	{
		string toRet = "";
		toRet += username;
		toRet += " Wrong answers: " + wrongAnswerCount.ToString();
		toRet += " Correct answers: " + correctAnswerCount.ToString();
		toRet += " Averege answer time: " + avarageAnswerTime.ToString();
		return toRet;
	}
}
public class RoomData
{
	public uint id { get; set; }
	public string name { get; set; }
	public uint maxPlayers { get; set; }
	public uint timePerQustion { get; set; }
	public uint isActive { get; set; }
	public uint questionCount { get; set; }

	public override string ToString()
	{
		string toRet = "ID: " + id.ToString();
		toRet += ", Name: " + name;
		toRet += ", max Players: " + maxPlayers.ToString();
		toRet += ", Time Per Question: " + timePerQustion.ToString();
		toRet += ", Question Count: " + questionCount.ToString();
		return toRet;
	}
}
public class ErrorResponse
{
	public string messege { get; set; }
}

public class LoginResponse
{
	public uint status { get; set; }
}

public class SignupResponse
{
	public uint status { get; set; }
}

public class LogoutRespone
{
	public uint status { get; set; }
}

public class getRoomsResponse
{
	public uint status { get; set; }
	public List<RoomData> rooms { get; set; }
}

public class getPlayerslnRoomRespones
{
	public List<string> players { get; set; }
}

public class joinRoomResponse
{
	public uint status { get; set; }
}

public class createRoomResponse
{
	public uint status { get; set; }
}

public class getStatisticsResponse
{
	public uint status { get; set; }
	public List<string> scores { get; set; }
}

public class closeRoomResponse
{
	public uint status { get; set; }
}

public class startGameResponse
{
	public uint status { get; set; }
}

public class getRoomStateRespone
{
	public uint status { get; set; }
	public bool hasGameBegun { get; set; }
	public List<string> players { get; set; }
	public uint questionCount { get; set; }
	public uint answerTimeout { get; set; }
}

public class LeaveRoomResponse
{
	public uint status { get; set; }
}

public class submitAnswerResponse
{
	public uint status { get; set; }
}

public class getGameResultsResponse
{
	public uint status { get; set; }
	public List<playerResults> results { get; set; }
}

public class GetQuestionResponse
{
	public uint status { get; set; }
	public string question { get; set; }
	public List<string> answers { get; set; }
}

public class CloseGameResponse
{
	public uint status { get; set; }
}

public class LeaveGameResponse
{
	public uint status { get; set; }
}

public class CorrectAnswerMessege
{
	public string answer { get; set; }
}

public class LoginRequest
{
	public string username { get; set; }
	public string password { get; set; }
}

public class SignupRequest
{
	public string username { get; set; }
	public string password { get; set; }
	public string email { get; set; }
}

public class getPlayerslnRoomRequest
{
	public uint roomId { get; set; }
}

public class joinRoomRequest
{
	public uint roomId { get; set; }
}

public class createRoomResquest
{
	public string roomName { get; set; }
	public uint maxUsers { get; set; }
	public uint questionsCount { get; set; }
	public uint answerTimeout { get; set; }
}

public class submitAnswerRequest
{
	public string answer { get; set; }
}

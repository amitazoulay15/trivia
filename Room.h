#pragma once
#include "LoggedUser.h"
#include <vector>
#include <iterator>
#include <mutex>
#include "JsonResponsePacketSerializer.h"

class Room
{
public:
	Room(RoomData metadata, LoggedUser* user);
	~Room();
	void addUser(LoggedUser* user);
	void removeUser(LoggedUser* user);
	std::vector<std::string> getallusers();
	RoomData* getData();
	std::vector<LoggedUser*> getAllLoggedUsers();
	void sendMessegeToRoomMembers(char* mes, LoggedUser* admin = nullptr);
	std::mutex* getMtx();
private:
	std::mutex users_mtx;
	std::vector<LoggedUser*> m_users;
	RoomData m_metadata;
};


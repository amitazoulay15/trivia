#pragma once
#include "IRequestHandler.h"
#include "Communicator.h"
#include "RequestHandlerFactory.h"

class GameAdminRequestHandler : public IRequestHandler
{
private:
	Game* m_game;
	LoggedUser* m_user;
	RequestResult submitAnswer(RequestInfo req);
	RequestResult closeGame(RequestInfo req);
public:
	GameAdminRequestHandler(Game* game, LoggedUser* user);
	~GameAdminRequestHandler();
	virtual bool isRequestRelevant(RequestInfo req) override;
	virtual RequestResult handleRequest(RequestInfo req) override;
};


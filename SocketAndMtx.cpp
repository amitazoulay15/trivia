#include "SocketAndMtx.h"
#include <iostream>

SocketAndMtx::SocketAndMtx(SOCKET* sock)
{
	m_sock = sock;
}

SocketAndMtx::~SocketAndMtx()
{
	m_mtx.lock();
	delete m_sock;
	m_mtx.unlock();
}

void SocketAndMtx::sendMsg(char* msg)
{
	int len = strlen(msg);
	m_mtx.lock();
	send(*m_sock, msg, len, 0);
	m_mtx.unlock();
	delete[] msg;
}

RequestInfo SocketAndMtx::recvMsgFromSock()
{
	RequestInfo info;
	char* code = new char[2];
	char* legthPart = new char[5];
	char* data3;
	code[1] = 0;
	legthPart[4] = 0;
	int length = 0;
	std::string len;
	recv(*m_sock, code, 1, 0);
	info.code = code[0];
	recv(*m_sock, legthPart, 4, 0);
	len = legthPart;
	if (stoi(len) > 0)
	{
		length = stoi(len) + 1;
		data3 = new char[length];
		data3[length - 1] = 0;
		recv(*m_sock, data3, length, 0);
		info.buffer = data3;
	}
	delete[] legthPart;
	return info;
}

#include "StatisticsManager.h"

StatisticsManager* StatisticsManager::instance = nullptr;

/*
this method is the constructor
intput: non
output: non
*/
StatisticsManager::StatisticsManager()
{
	m_database = SqliteDataBase::getInstance();//initializing the pointer to the data base
}

/*
this method is for the singelton part
input: non
output: StatisticsManager*, pointer to the only instance of the class
*/
StatisticsManager* StatisticsManager::getInstance()
{
	if (instance == nullptr) instance = new StatisticsManager();
	return instance;
}

/*
this method is the D'tor
input: non
output: non
*/
StatisticsManager::~StatisticsManager()
{
}

/*
this method builda all the needed statistics for the 5 best player and the player who asked the statistics
input: the username of the player, string
output: non
*/
std::vector<std::string> StatisticsManager::getStatistics(std::string user)
{
	std::vector<std::string> toRet = this->m_database->getBestPlayers();//getting the best players
	while (toRet.size() < BEST_PLAYERS_AMOUNT)
		toRet.push_back("No One!");
	toRet.push_back(user + " score: " + std::to_string(this->m_database->getUserStatistics(user)));//adding the score of the player
	return toRet;
}

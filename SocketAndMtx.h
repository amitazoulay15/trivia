#pragma once
#include <WinSock2.h>
#include <mutex>
#include <vector>
#include "defines.h"
#include "structs.h"

class SocketAndMtx
{
public:
	SocketAndMtx(SOCKET* sock);
	~SocketAndMtx();
	void sendMsg(char* msg);
	RequestInfo recvMsgFromSock();
private:
	SOCKET* m_sock;
	std::mutex m_mtx;
};


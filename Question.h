#pragma once
#include <string>
#include <vector>

class Question
{
public:
	Question(std::string question, std::string correct, std::vector<std::string> possibleAnswers);
	~Question();
	std::string getQuestion();
	std::vector<std::string> getPossibleAnswers();
	std::string	getCorrentAnswer();
private:
	std::string m_question;
	std::string m_correct;
	std::vector<std::string> m_possibleAnswers;
};


#include "LoggedUser.h"

/*
this method is the C'tor
input: string, the username
output: non
*/
LoggedUser::LoggedUser(std::string username)
{
	this->m_username = username;
	this->m_sock = nullptr;
}

/*
this method the C'tor
input: string, the username, SOCKET*, pointer to the socket the user is on
output: non
*/
LoggedUser::LoggedUser(std::string username, SocketAndMtx* sock)
{
	this->m_username = username;
	this->m_sock = sock;
}

/*
this method sets the username of the user
input: string, the username
output: non
*/
void LoggedUser::setName(std::string username)
{
	this->m_username = username;
}

/*
this method is the default C'tor
input: non
output: non
*/
LoggedUser::LoggedUser()
{
	this->m_sock = nullptr;
}

/*
this method is the D'tor
input: non
output: non
*/
LoggedUser::~LoggedUser()
{
}

/*
this method return the username of the user
input: non
output: string, the username
*/
std::string LoggedUser::getUsername()
{
	return this->m_username;
}

/*
this method send a messege to the client
input: char*, the messege
output: non
*/
void LoggedUser::sendMessege(char* messege)
{
	m_sock->sendMsg(messege);
}

/*
this method return the socket of the client
input: non
output: SOCKET*, the socket of the client
*/
SocketAndMtx* LoggedUser::getSocket()
{
	return this->m_sock;
}

/*
this method sets the socket of the user
input: SOCKET*, the new socket
output: non
*/
void LoggedUser::setSocket(SocketAndMtx* sock)
{
	this->m_sock = sock;
}

#pragma once
#include "structs.h"
#include "json.hpp"
#include "defines.h"

class JsonRequestPacketDeserializer
{
public:
	static LoginRequest deserializeLoginRequest(char* msg);
	static SignupRequest deserializeSignUpRequest(char* msg);
	static getPlayerslnRoomRequest deserializeGetPlayersRequest(char* msg);
	static joinRoomRequest deserializeJoinRoomRequest(char* msg);
	static createRoomResquest deserializeCreateRoomRequest(char* msg);
	static submitAnswerRequest deserializeSubmitAnswerRequest(char* msg);
};


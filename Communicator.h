#pragma once
#include "RequestHandlerFactory.h"
#include "SocketAndMtx.h"
#include <iostream>

class Communicator
{
private:
	static Communicator* instance;
	std::pair <bool, std::mutex> m_work;
	std::mutex m_client_mtx;
	std::map<SocketAndMtx*, IRequestHandler*> m_clients;
	SOCKET m_serverSock;
	void bindAndListen();
	static void handleNewClient(std::map<SocketAndMtx*, IRequestHandler*>& clients, SocketAndMtx* sock, std::mutex& mtx);
	static void cleanUsers(std::map<SocketAndMtx*, IRequestHandler*>& clients, std::mutex& mtx, std::pair <bool, std::mutex>& m_work);
	void accept();
	Communicator();
public:
	~Communicator();
	void updateRequestHandler(SocketAndMtx* sock, IRequestHandler* newHandler);
	SocketAndMtx* getSock(IRequestHandler* handler);
	static Communicator* getInstance();
	void startHandleRequests();
};
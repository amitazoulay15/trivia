#include "LoginRequestHandler.h"

/*
this method handles the login request
input: RequestInfo, information about the request
output: RequestResult, the handle response
*/
RequestResult LoginRequestHandler::login(RequestInfo req)
{
	LoginManager* loginManager = LoginManager::getInstance();
	RequestResult toRet;
	LoginRequest loginReq = JsonRequestPacketDeserializer::deserializeLoginRequest(req.buffer);
	LoginResponse res;
	m_user = loginManager->login(loginReq.username, loginReq.password);
	m_user->setSocket(Communicator::getInstance()->getSock(this));
	res.status = OK;
	toRet.response = JsonResponsePacketSerializer::serializeResponse(res);
	return toRet;
}

/*
this method handles the signup request
input: RequestInfo, information about the request
output: RequestResult, the handle response
*/
RequestResult LoginRequestHandler::signup(RequestInfo req)
{
	LoginManager* loginManager = LoginManager::getInstance();
	RequestResult toRet;
	SignupRequest signupReq = JsonRequestPacketDeserializer::deserializeSignUpRequest(req.buffer);
	SignupResponse res;
	m_user = loginManager->signup(signupReq.username, signupReq.password, signupReq.email);
	m_user->setSocket(Communicator::getInstance()->getSock(this));
	res.status = OK;
	toRet.response = JsonResponsePacketSerializer::serializeResponse(res);
	return toRet;
}

/*
this method is the C'tor
input: non
output: non
*/
LoginRequestHandler::LoginRequestHandler()
{
	m_user = nullptr;
}

/*
this method is the D'tor
input: non
output: non
*/
LoginRequestHandler::~LoginRequestHandler()
{
}

bool LoginRequestHandler::isRequestRelevant(RequestInfo req)
{
	char code = req.code;
	return (code == LOGIN_CODE || code == SIGNUP_CODE);
}

RequestResult LoginRequestHandler::handleRequest(RequestInfo req)
{
	RequestResult toRet;
	try
	{
		if (req.code == LOGIN_CODE) toRet = login(req);
		else toRet = signup(req);
		toRet.newHandler = RequestHandlerFactory::CreateMenuRequestHandler(m_user);
	}
	catch (std::exception& e)
	{
		toRet.response = JsonResponsePacketSerializer::buildErrorMsg(e.what());
		toRet.newHandler = this;
	}
	return toRet;
}

#pragma once
#include "sqliteDataBase.h"
#include <iterator>
#include <algorithm>

class StatisticsManager
{
private:
	static StatisticsManager* instance;
	SqliteDataBase* m_database;
	StatisticsManager();
public:
	static StatisticsManager* getInstance();
	~StatisticsManager();
	std::vector<std::string> getStatistics(std::string user);
};


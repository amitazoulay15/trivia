#pragma once
#include "Question.h"
#include "LoggedUser.h"
#include "structs.h"
#include "SqliteDataBase.h"
#include "JsonResponsePacketSerializer.h"
#include <vector>
#include <map>
#include <mutex>

class Game
{
public:
	Game(std::vector<LoggedUser*> users, RoomData data);
	~Game();
	void submitAnswer(LoggedUser* user, std::string ans, clock_t time);
	void removeUser(LoggedUser* user);
	std::vector<LoggedUser*> getUsers();
	std::map<LoggedUser*, GameData*> getUsersMap();
	void sendMessegeToGameMembers(std::string messege, LoggedUser* sender = nullptr);
	std::mutex* getMtx();
	void start();
private:
	std::vector<playerResults> getGameResults();
	void updateScores();
	static char* buildQuestionResp(Question* q);
	static char* buildGameResultsResp(Game* game);
	static void runGame(Game* game);
	int m_timePerQ;
	std::mutex players_mtx;
	std::map<LoggedUser*,GameData*> m_players_data;
	std::vector<Question*> m_questions;
	std::pair<int, std::mutex> m_questionPlacePair;
	std::pair<clock_t, std::mutex> m_timePair;
	std::thread* m_gameControl;
};


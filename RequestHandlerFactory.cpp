#include "RequestHandlerFactory.h"

IRequestHandler* RequestHandlerFactory::CreateLoginRequestHandler()
{
	return new LoginRequestHandler();
}

IRequestHandler* RequestHandlerFactory::CreateMenuRequestHandler(LoggedUser* m_user)
{
	return new MenuRequestHandler(m_user);
}

IRequestHandler* RequestHandlerFactory::CreateRoomMemRequestHandler(Room* room, LoggedUser* user)
{
	return new RoomMemberRequestHandler(room, user);
}

IRequestHandler* RequestHandlerFactory::CreateRoomAdRequestHandler(Room* room, LoggedUser* user)
{
	return new RoomAdminRequestHandler(room, user);
}

IRequestHandler* RequestHandlerFactory::CreateGameMemRequestHandler(Game* game, LoggedUser* user)
{
	return new GameMemberRequestHandler(game, user);
}

IRequestHandler* RequestHandlerFactory::CreateGameAdRequestHandler(Game* game, LoggedUser* user)
{
	return new GameAdminRequestHandler(game, user);
}

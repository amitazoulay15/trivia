#include "SqliteDataBase.h"

SqliteDataBase* SqliteDataBase::instance = nullptr;

/*
this method is the C'tor
intput: non
output: non
*/
SqliteDataBase::SqliteDataBase()
{
	openDB();//opening the DB
}

/*
this method is a callback
intput: void* data, int argc, char** argv, char** azColName
output: int
*/
int SqliteDataBase::callGetUserQsAmount(void* data, int argc, char** argv, char** azColName)
{
	int* q = (int*)data;
	*q = std::stoi(argv[0]);//changing the first(and only) output to int
	return 0;
}

/*
this method is a callback
intput: void* data, int argc, char** argv, char** azColName
output: int
*/
int SqliteDataBase::callGetQuestion(void* data, int argc, char** argv, char** azColName)
{
	int i = 0;
	Question** q = (Question**)data;
	std::string qu, a;
	std::vector<std::string> fakeAns;
	for (; i < argc; i++)
	{
		if (std::string(azColName[i]) == "QUESTION")
		{
			qu = argv[i];
		}
		else if (std::string(azColName[i]) == "A1")
		{
			fakeAns.push_back(argv[i]);
		}
		else if (std::string(azColName[i]) == "A2")
		{
			fakeAns.push_back(argv[i]);
		}
		else if (std::string(azColName[i]) == "A3")
		{
			fakeAns.push_back(argv[i]);
		}
		else
		{
			a = argv[i];
		}
	}
	*q = new Question(qu, a, fakeAns);
	return 0;
}

/*
this method is a callback
intput: void* data, int argc, char** argv, char** azColName
output: int
*/
int SqliteDataBase::callCountQuestions(void* data, int argc, char** argv, char** azColName)
{
	int* a = (int*)data;
	for (int i = 0; i < argc; i++)
	{
		*a = std::stoi(argv[i]);
	}
	return 0;
}

/*
this method the D'tor
intput: non
output: non
*/
SqliteDataBase::~SqliteDataBase()
{
	closeDB();//closing the DB
}

/*
this method is a callback to build a vector of users
intput: void* data, int argc, char** argv, char** azColName
output: int
*/
int SqliteDataBase::callbackGetUsers(void* data, int argc, char** argv, char** azColName)
{
	int id = 0;
	std::string name;
	std::vector<std::string>* users = (std::vector<std::string>*)data;
	for (int i = 0; i < argc; i++) 
	{
		name = argv[i];
		users->push_back(name);
	}
	return 0;
}

/*
this method is a callback takes only the username and score (these are the only things im asking for in the sql request)
intput: void* data, int argc, char** argv, char** azColName
output: int
*/
int SqliteDataBase::callBackGetBestPlayers(void* data, int argc, char** argv, char** azColName)
{
	//callback to build a vector of users from the output of the sql resuest
	int id = 0;
	std::string name;
	float score;
	std::vector<std::string>* users = (std::vector<std::string>*)data;
	for (int i = 0; i < argc; i++)
	{
		if (std::string(azColName[i]) == "USERNAME")
		{
			name = argv[i];
		}
		else
		{
			score = std::stof(argv[i]);
			users->push_back(name + " score: " + std::to_string(score));
		}
	}
	return 0;
}

/*
this method run a command in the data base
intput: string, the command
output: non
*/
void SqliteDataBase::runCommand(std::string cm)
{
	char** errMessage = nullptr;
	int res = sqlite3_exec(db, cm.c_str(), nullptr, nullptr, errMessage);//running a command on the sql db 
	if (res != SQLITE_OK) std::cout << ERROR_SQL;
}

/*
this method opens the DB
intput: non
output: non
*/
void SqliteDataBase::openDB()
{
	std::string dbFileName = "MyDB.sqlite";
	int doesFileExist = _access(dbFileName.c_str(), 0);
	int res = sqlite3_open(dbFileName.c_str(), &db);
	if (res != SQLITE_OK)
	{
		db = nullptr;
	}
	if (doesFileExist != 0)//if file doesnt exists initialize it after creating it
	{
		std::string sqlStatement[] = {
			"CREATE TABLE USERS (USERNAME TEXT PRIMARY KEY NOT NULL, PASSWORD TEXT NOT NULL, EMAIL TEXT NOT NULL, QUESTIONS INTEGER, CORRECT_QUESTIONS INTEGER, SCORE TEXT)",
			"CREATE TABLE QUESTIONS (ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, QUESTION TEXT NOT NULL, A1 TEXT NOT NULL, A2 TEXT NOT NULL, A3 TEXT NOT NULL, ANSWER TEXT NOT NULL)"
		};//the sql statements i need to send
		for (int i = 0; i < 2; i++)//running all the commands to init the sql 
		{
			runCommand(sqlStatement[i]);
		}
	}
}

/*
this method closes the DB
intput: non
output: non
*/
void SqliteDataBase::closeDB()
{
	sqlite3_close(db);//closing the DB
	db = nullptr;
}

/*
this method gets the questions amount
intput: non
output: int, the questions amount
*/
int SqliteDataBase::getQuestionAmount()
{
	std::string sqlStatement = "SELECT ID FROM QUESTIONS ORDER BY ID DESC LIMIT 1;";
	char* errMessage = nullptr;
	int toRet;
	int res = sqlite3_exec(db, sqlStatement.c_str(), callCountQuestions, &toRet, &errMessage);
	if (res != SQLITE_OK) std::cout << ERROR_SQL;
	return toRet;
}

/*
this method gets the amount of correct answer the user has
intput: the username of the player
output: int(amount of correct questions)
*/
int SqliteDataBase::getUserCorrectQuestions(std::string username)
{
	std::string sqlStatement = "SELECT CORRECT_QUESTIONS FROM USERS WHERE USERNAME = '" + username + "';";
	char* errMessage = nullptr;
	int toRet = 0;
	int res = sqlite3_exec(db, sqlStatement.c_str(), callGetUserQsAmount, &toRet, &errMessage);
	if (res != SQLITE_OK) std::cout << ERROR_SQL;
	return toRet;
}

/*
this method gets the amount of questions the user answered
intput: the username of the player
output: int(amount of questions answered)
*/
int SqliteDataBase::getUserOverallQuestions(std::string username)
{
	std::string sqlStatement = "SELECT QUESTIONS FROM USERS WHERE USERNAME = '" + username + "';";
	char* errMessage = nullptr;
	int toRet = 0;
	int res = sqlite3_exec(db, sqlStatement.c_str(), callGetUserQsAmount, &toRet, &errMessage);
	if (res != SQLITE_OK) std::cout << ERROR_SQL;
	return toRet;
}

/*
this method check if the user exists in the data base
intput: the username of the player
output: bool, does the user exists
*/
bool SqliteDataBase::doesUserExist(std::string user)
{
	std::string sqlStatement = "SELECT USERNAME FROM USERS WHERE USERNAME = '" + user + "';";//getting the user
	char* errMessage = nullptr;
	std::vector<std::string> users;
	int res = sqlite3_exec(db, sqlStatement.c_str(), callbackGetUsers, &users, &errMessage);
	if (res != SQLITE_OK) throw std::exception("error running command");
	bool toRet = (users.size() > 0);//checking the length of all the returned user if bigger than 0 the user exists
	return toRet;
}

/*
this method check if the password matches the username
intput: 2 strings, the username and the password
output: bool, does the password match
*/
bool SqliteDataBase::doesPasswordMatch(std::string user, std::string password)
{
	std::string sqlStatement = "SELECT USERNAME FROM USERS WHERE USERNAME = '" + user + "' AND PASSWORD = '" + password + "';";//getting the user
	char* errMessage = nullptr;
	std::vector<std::string> users;
	int res = sqlite3_exec(db, sqlStatement.c_str(), callbackGetUsers, &users, &errMessage);
	if (res != SQLITE_OK) std::cout << ERROR_SQL;
	bool toRet = (users.size() > 0);//checking the length of all the returned user if bigger than 0 the user exists
	return toRet;
}

/*
this method adds new user to the DB
intput: 3 strings, the username, the password and the email
output: non
*/
void SqliteDataBase::addNewUser(std::string username, std::string password, std::string email)
{
	char* errMessage = nullptr;
	std::string sqlStatement ("INSERT INTO USERS(USERNAME, PASSWORD, EMAIL, QUESTIONS, CORRECT_QUESTIONS, SCORE) VALUES('" + username + "','" + password + "','" + email + "', 0, 0, '0');");
	int res = sqlite3_exec(this->db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)  std::cout << ERROR_SQL;
}

/*
this method gets the statistics of a user
intput: string, the username
output: the player's score													
*/
float SqliteDataBase::getUserStatistics(std::string user)
{
	std::string sqlStatement = "SELECT SCORE FROM USERS WHERE USERNAME = '" + user + "';";//getting the score of the user
	char* errMessage = nullptr;
	std::vector<std::string> score;
	int res = sqlite3_exec(db, sqlStatement.c_str(), callbackGetUsers, &score, &errMessage);
	if (res != SQLITE_OK) std::cout << ERROR_SQL;
	if (score.size() > 0) return std::stof(score[0]);//checking the length of all the returned score if bigger than 0, the user exists
	std::cout << ERROR_SQL;
	return 0.0;
}

/*																																																						 
this method builds a vector of string that contains data ab
intput: non
output: vector of string, data about each player
*/
std::vector<std::string> SqliteDataBase::getBestPlayers()
{
	std::string sqlStatement = "SELECT USERNAME, SCORE FROM USERS ORDER BY SCORE DESC LIMIT 5;";//getting the scores of all users
	char* errMessage = nullptr;
	std::vector<std::string> scores;
	int res = sqlite3_exec(db, sqlStatement.c_str(), callBackGetBestPlayers, &scores, &errMessage);
	if (res != SQLITE_OK) std::cout << ERROR_SQL;
	return scores;
}

/*
this method builds a question
intput: non
output: pointer to question
*/
Question* SqliteDataBase::getQuestion()
{
	int id = rand() % getQuestionAmount() + 1;
	std::string sqlStatement = "SELECT QUESTION, A1, A2, A3, ANSWER FROM QUESTIONS WHERE ID = " + std::to_string(id) + ";";
	char* errMessage = nullptr;
	Question* toRet;
	int res = sqlite3_exec(db, sqlStatement.c_str(), callGetQuestion, &toRet, &errMessage);
	if (res != SQLITE_OK) std::cout << ERROR_SQL;
	return toRet;
}

/*
this method updates the correct answer count for a user
intput: string, the username of the player
output: non
*/
void SqliteDataBase::updateCorrectAnswersOfUser(std::string username)
{
	char* errMessage = nullptr;
	std::string sqlStatement = "UPDATE USERS SET CORRECT_QUESTIONS = CORRECT_QUESTIONS + 1 WHERE USERNAME = '" + username + "';";
	int res = sqlite3_exec(this->db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK) std::cout << ERROR_SQL;
}

/*
this method updates the overall questions a user answered
intput: string, the username of the player
output: non
*/
void SqliteDataBase::updateOverAllQuestions(std::string username)
{
	char* errMessage = nullptr;
	std::string sqlStatement = "UPDATE USERS SET QUESTIONS = QUESTIONS + 1 WHERE USERNAME = '" + username + "';";
	int res = sqlite3_exec(this->db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK) std::cout << ERROR_SQL;
}

/*
this method updates the score of a user
intput: string, the username of the player
output: non
*/
void SqliteDataBase::updateScoreOfUser(std::string username)
{
	char* errMessage = nullptr;
	std::string scoreAsString = std::to_string((float)getUserCorrectQuestions(username) / (float)getUserOverallQuestions(username));
	std::string sqlStatement = "UPDATE USERS SET SCORE = '" + scoreAsString + "' WHERE USERNAME = '" + username + "';";
	int res = sqlite3_exec(this->db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK) std::cout << ERROR_SQL;
}

/*
this method is for the singelton
intput: non
output: SqliteDataBase*, pointer to the only instance of the class
*/
SqliteDataBase* SqliteDataBase::getInstance()
{
	if (instance == nullptr) instance = new SqliteDataBase();
	return instance;
}

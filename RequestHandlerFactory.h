#pragma once
#include "RoomAdminRequestHandler.h"
#include "GameAdminRequestHandler.h"
#include "RoomMemberRequestHandler.h"
#include "GameMemberRequestHandler.h"
#include "MenuRequestHandler.h"
#include "LoginRequestHandler.h"

class RequestHandlerFactory
{
public:
	static IRequestHandler* CreateLoginRequestHandler();
	static IRequestHandler* CreateMenuRequestHandler(LoggedUser* m_user);
	static IRequestHandler* CreateRoomMemRequestHandler(Room* room, LoggedUser* user);
	static IRequestHandler* CreateRoomAdRequestHandler(Room* room, LoggedUser* user);
	static IRequestHandler* CreateGameMemRequestHandler(Game* game, LoggedUser* user);
	static IRequestHandler* CreateGameAdRequestHandler(Game* game, LoggedUser* user);
};


#pragma once
#include "IRequestHandler.h"
#include "Communicator.h"
#include "RequestHandlerFactory.h"

class RoomAdminRequestHandler : public IRequestHandler
{
private:
	Room* m_room;
	LoggedUser* m_user;
	RequestResult closeRoom(RequestInfo req);
	RequestResult startGame(RequestInfo req);
	RequestResult getRoomState(RequestInfo req);
public:
	RoomAdminRequestHandler(Room* room, LoggedUser* user);
	~RoomAdminRequestHandler();
	virtual bool isRequestRelevant(RequestInfo req) override;
	virtual RequestResult handleRequest(RequestInfo req) override;
};


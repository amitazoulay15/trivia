#pragma once
#include "sqlite3.h"
#include <iostream>
#include <io.h>
#include <sstream>
#include <vector>
#include <map>
#include "defines.h"
#include "Question.h"
#include <mutex>
#include <algorithm>

class SqliteDataBase
{
public:
	~SqliteDataBase();
	bool doesUserExist(std::string user);
	bool doesPasswordMatch(std::string user, std::string password);
	void addNewUser(std::string username, std::string password, std::string email);
	float getUserStatistics(std::string user);
	std::vector<std::string> getBestPlayers();
	Question* getQuestion();
	void updateCorrectAnswersOfUser(std::string username);
	void updateOverAllQuestions(std::string username);
	void updateScoreOfUser(std::string username);
	static SqliteDataBase* getInstance();
private:
	SqliteDataBase();
	static int callGetUserQsAmount(void* data, int argc, char** argv, char** azColName);
	static int callGetQuestion(void* data, int argc, char** argv, char** azColName);
	static int callCountQuestions(void* data, int argc, char** argv, char** azColName);
	static int callbackGetUsers(void* data, int argc, char** argv, char** azColName);
	static int callBackGetBestPlayers(void* data, int argc, char** argv, char** azColName);
	void runCommand(std::string cm);
	void openDB();
	void closeDB();
	int getQuestionAmount();
	int getUserCorrectQuestions(std::string username);
	int getUserOverallQuestions(std::string username);
	sqlite3* db;
	static SqliteDataBase* instance;
};


#include "Server.h"

Server* Server::instance = nullptr;

/*
this method calls the communicator that listens on the port
input: non
output: non
*/
void Server::callCommunicator()
{
	Communicator::getInstance()->startHandleRequests();
}

/*
this method closes all the resurces the server is using
input: non
output: non
*/
void Server::closeAllResurces()
{
	delete m_communicator; //error for some reason
}

/*
this method is the C'tor
input: non
output: non
*/
Server::Server()
{
	//initializing all the singletons
	m_communicator = Communicator::getInstance();
	LoginManager::getInstance();
	GameManager::getInstance();
	LoginManager::getInstance();
	RoomManager::getInstance();
	StatisticsManager::getInstance();
	SqliteDataBase::getInstance();
}

/*
this method is for the singelton
input: non
output: Server*, pointer to the only instance of the class
*/
Server* Server::getInstance()
{
	if (instance == nullptr) instance = new Server();
	return instance;
}

/*
this method is the D'tor
input: non
output: non
*/
Server::~Server()
{
}

/*
this method starts the run of all the functions and threads that takes care of clients and waits for commands from the server owner
input: non
output: non
*/
void Server::run()
{
	std::thread t_connector(callCommunicator);
	t_connector.detach();
	std::string command;
	do
	{
		std::cin >> command;
	} while (command != STOP_CMD);
	closeAllResurces();//closing all the used resurces
}

#include "Game.h"

/*
this method is the C'tor
input: vector of all the user that the game will contain, RoomData, some data the game need
output: non
*/
Game::Game(std::vector<LoggedUser*> users, RoomData data)
{
	m_timePerQ = data.timePerQustion;
	players_mtx.lock();
	for (int i = 0; i < users.size(); i++)
	{
		GameData* data = new GameData();
		data->correctAnswerCount = 0;
		data->wrongAnswerCount = 0;
		data->averageAnswerTime = 0;
		data->correct = false;
		data->onTime = false;
		m_players_data.insert(std::pair<LoggedUser*, GameData*>(users[i], data));
	}
	players_mtx.unlock();
	SqliteDataBase* sqDB = SqliteDataBase::getInstance();
	for (int i = 0; i < data.questionCount; i++)
		m_questions.push_back(sqDB->getQuestion());
	m_questionPlacePair.first = 0;
	m_gameControl = nullptr;
}

/*
this method is the D'tor
input: non
output: non
*/
Game::~Game()
{
	players_mtx.lock();
	for (std::map<LoggedUser*, GameData*>::iterator i = m_players_data.begin(); i != m_players_data.end(); ++i)
		delete i->second;
	players_mtx.unlock();
	delete m_gameControl;
}

/*
this method check the logics of submit answer
input: LoggedUser*, the user that submite the answer, string, the ansswer the user sent,
clock_t, the time the answer was recived at
output: non
*/
void Game::submitAnswer(LoggedUser* user, std::string ans, clock_t time)
{
	int timeInSec = 0;
	int q = 0;
	clock_t ansTime = time;
	bool goodTime = false, correctAns = false;
	m_timePair.second.lock();
	timeInSec = (int)((((double)ansTime - m_timePair.first) / (double)CLOCKS_PER_SEC));
	if (timeInSec <= m_timePerQ) goodTime = true;//checking if the time is good
	m_timePair.second.unlock();
	if (goodTime)
	{
		m_questionPlacePair.second.lock();
		q = m_questionPlacePair.first;
		m_questionPlacePair.second.unlock();
		players_mtx.lock();
		m_players_data[user]->onTime = true;
		m_players_data[user]->averageAnswerTime += timeInSec;
		if (m_questions[q]->getCorrentAnswer() == ans)//checking if the answer is correct
			m_players_data[user]->correct = true;
		players_mtx.unlock();
	}
}

/*
this method removes a user from the game
input: LoggedUser*, the user to remove
output: non
*/
void Game::removeUser(LoggedUser* user)
{
	players_mtx.lock();
	delete m_players_data[user];
	m_players_data.erase(user);
	players_mtx.unlock();
	std::string msg = user->getUsername() + " just left the game!" + POPUP_MESSEGE;
	char* messege = JsonResponsePacketSerializer::buildErrorMsg(msg);
	this->sendMessegeToGameMembers(messege);
}

/*
this method builds a question messege to send to the players
input: Question*, the question
output: char*, the messege
*/
char* Game::buildQuestionResp(Question* q)
{
	GetQuestionResponse resp;
	resp.status = OK;
	resp.question = q->getQuestion();
	resp.answers = q->getPossibleAnswers();
	resp.answers.push_back(q->getCorrentAnswer());
	return JsonResponsePacketSerializer::serializeResponse(resp);
}

/*
this method build a Game Results Response
input: Game*, the game
output: char*, the messege
*/
char* Game::buildGameResultsResp(Game* game)
{
	getGameResultsResponse resp;
	resp.status = OK;
	resp.results = game->getGameResults();
	return JsonResponsePacketSerializer::serializeResponse(resp);
}

/*
this method builds a vector of all the player resaults
input: non
output: std::vector<playerResults>, vector of the player resaults
*/
std::vector<playerResults> Game::getGameResults()
{
	std::vector<playerResults> toRet;
	players_mtx.lock();
	for (std::map<LoggedUser*, GameData*>::iterator i = m_players_data.begin(); i != m_players_data.end(); ++i)
	{
		playerResults toAdd;
		if (m_questions.size() == 0)
			toAdd.avarageAnswerTime = 0;
		else
			toAdd.avarageAnswerTime = i->second->averageAnswerTime / m_questions.size();
		toAdd.correctAnswerCount = i->second->correctAnswerCount;
		toAdd.wrongAnswerCount = i->second->wrongAnswerCount;
		toAdd.username = i->first->getUsername();
		toRet.push_back(toAdd);
	}
	players_mtx.unlock();
	return toRet;
}

/*
this method updates the scores of all the players
input: non
output: non
*/
void Game::updateScores()
{
	SqliteDataBase* dataBase = SqliteDataBase::getInstance();
	players_mtx.lock();
	for (std::map<LoggedUser*, GameData*>::iterator i = m_players_data.begin(); i != m_players_data.end(); ++i)
	{
		if (i->second->onTime)
		{
			if (i->second->correct)
			{
				i->second->correctAnswerCount++;
				dataBase->updateCorrectAnswersOfUser(i->first->getUsername());
			}
		}
		else
		{
			i->second->averageAnswerTime += m_timePerQ;
		}
		if (!i->second->correct || !i->second->onTime)
			i->second->wrongAnswerCount++;
		dataBase->updateOverAllQuestions(i->first->getUsername());
		dataBase->updateScoreOfUser(i->first->getUsername());
		i->second->onTime = false;
		i->second->correct = false;
	}
	players_mtx.unlock();
}

/*
this method builds a vector of all the logged users in the game
input: non
output: std::vector<LoggedUser*>, vector of all the logged users in the game
*/
std::vector<LoggedUser*> Game::getUsers()
{
	std::vector<LoggedUser*> toRet;
	players_mtx.lock();
	for (std::map<LoggedUser*, GameData*>::iterator i = m_players_data.begin(); i != m_players_data.end(); ++i)
		toRet.push_back(i->first);
	players_mtx.unlock();
	return toRet;
}

std::map<LoggedUser*, GameData*> Game::getUsersMap()
{
	return m_players_data;
}

/*
this method send a messege to all the players in the game
input: string, the messege to send
output: non
*/
void Game::sendMessegeToGameMembers(std::string messege, LoggedUser* sender)
{
	players_mtx.lock();
	for (std::map<LoggedUser*, GameData*>::iterator i = m_players_data.begin();
		i != m_players_data.end(); ++i)
	{
		if (i->first != sender)
		{
			char* send = new char[messege.length() + 1];
			send[messege.length()] = 0;
			for (int i = 0; i < messege.length(); i++)
				send[i] = messege[i];
			i->first->sendMessege(send);
		}
	}
	players_mtx.unlock();
}

std::mutex* Game::getMtx()
{
	return &players_mtx;
}

void Game::start()
{
	m_gameControl = new std::thread(Game::runGame, this);
	m_gameControl->detach();
}

/*
this method runs the game
input: pointer to the game it runs
output: non
*/
void Game::runGame(Game* game)
{
	std::vector<Question*>& questions = game->m_questions;
	std::pair<int, std::mutex>& questionPlacePair = game->m_questionPlacePair;
	std::pair<clock_t, std::mutex>& timePair = game->m_timePair;
	int timePerQ = game->m_timePerQ;
	char* send;
	for (int i = 0; i < questions.size(); i++)
	{
		questionPlacePair.second.lock();
		questionPlacePair.first = i;
		questionPlacePair.second.unlock();
		send = buildQuestionResp(questions[i]);
		game->sendMessegeToGameMembers(send);
		timePair.second.lock();
		timePair.first = clock();//taking the sending time
		timePair.second.unlock();
		Sleep(timePerQ * SECONDS_TO_MILI);
		game->updateScores();
	}
	send = buildGameResultsResp(game);
	game->sendMessegeToGameMembers(send);
}